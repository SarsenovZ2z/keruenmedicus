<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\VacancyRequest as StoreRequest;
use App\Http\Requests\VacancyRequest as UpdateRequest;

/**
 * Class VacancyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class VacancyCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Vacancy');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/vacancy');
        $this->crud->setEntityNameStrings('vacancy', 'vacancies');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->addColumn([
            'name' => 'title',
            'label' => 'Название',
        ]);
        $this->crud->addColumn([
            'name' => 'experience',
            'label' => 'Опыт работы',
        ]);
        $this->crud->addColumn([
            'name' => 'salary',
            'label' => 'Заработная плата',
        ]);
        $this->crud->addColumn([
            'name' => 'active',
            'label' => 'Опубликовать',
            'type' => 'boolean',
        ]);

        $this->crud->addField([
            'name' => 'title',
            'label' => 'Название',
        ]);
        $this->crud->addField([
            'name' => 'experience',
            'label' => 'Опыт работы',
        ]);
        $this->crud->addField([
            'name' => 'salary',
            'label' => 'Заработная плата',
        ]);
        $this->crud->addField([
            'name' => 'active',
            'label' => 'Опубликовать',
            'type' => 'checkbox',
        ]);
        $this->crud->addField([
            'name' => 'duties',
            'label' => 'Обязанности',
            'type' => 'table',
            'fake' => true,
            'store_in' => 'extras',
            'entity_singular' => 'option',
            'columns' => [
                'content' => '',
            ],
        ]);
        $this->crud->addField([
            'name' => 'conditions',
            'label' => 'Условия',
            'type' => 'table',
            'fake' => true,
            'store_in' => 'extras',
            'entity_singular' => 'option',
            'columns' => [
                'content' => '',
            ],
        ]);
        $this->crud->addField([
            'name' => 'requirements',
            'label' => 'Требования',
            'type' => 'table',
            'fake' => true,
            'store_in' => 'extras',
            'entity_singular' => 'option',
            'columns' => [
                'content' => '',
            ],
        ]);


        // add asterisk for fields that are required in VacancyRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
