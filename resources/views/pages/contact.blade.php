@extends('master') 

@section('content')
<main>
    <div class="contact-page">
        <div class="row">
            <div class="col-sm-4">
                <div class="con-data">
                    <h2>КОНТАКТЫ</h2>
                    <h5>Адрес</h5>
                <p>{!! $page->address !!}</p>
                    <h5>Телефон</h5>
                    <div class="email-contact">
                        {!! $page->phones !!}
                    </div>
                    <h5>E-mail</h5>
                    <div class="email-contact">
                        {!! $page->email !!}
                    </div>
					<div class="social-con">
						<a href="https://itunes.apple.com/tr/app/keruen-medicus/id1380159640?l=tr&amp;mt=8">
						  <img src="images/apple.png" alt="">
						</a>
						<a href="https://play.google.com/store/apps/details?id=kz.mars.studio.keruen">
						  <img src="images/google-play.png" alt="">
						</a>
						<a href="https://www.facebook.com/KeruenmedicusAlmaty/">
						  <img src="images/face.png" alt="">
						</a>
						<a href="https://www.instagram.com/keruenmedicus/">
						  <img src="images/insta.png" alt="">
						</a>
					  </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div id="map" width="700px"></div>
            </div>
        </div>
    </div>
</main>
@endsection @section('after_jquery')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALuXuyukxHwZT8Va_yQC-DmIZ6r2fadgM">
</script>

<script type="text/javascript">
    var locations = [
    ['г. Алматы, бул. Бухар Жырау, 45/1', 43.231611,76.914929,17],
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: new google.maps.LatLng(43.231611,76.914929,17),
        mapTypeId: google.maps.MapTypeId.TERRAIN
    });

    var infowindow = new google.maps.InfoWindow({maxWidth: 175});
    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });
        infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
        })(marker, i));


    }
</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">

@endsection