@extends('master')

@section('content')
<main>
    <div class="row sale-txt">
      <div class="col-sm-6">
        <h1>Акции и специальные предложения</h1>
      </div>
      <div class="col-sm-6">
        <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
      </div>
    </div>
    <div class="row sales">
      <div class="col-sm-4 blue-back">
        <a href="inner-sale.html">
          <div class="sales-desc">
            <h3>-25%</h3>
            <p>Диагностика для детей</p>
          </div>
        </a>
      </div>
      <div class="col-sm-8">
        <a href="inner-sale.html"><img src="images/sale1.png" alt=""></a>
      </div>
    </div>
    <div class="row sales">
      <div class="col-sm-4 green-back">
        <a href="inner-sale.html">
          <div class="sales-desc">
            <h3>-15%</h3>
            <p>Танцы для беременных</p>
          </div>
        </a>
      </div>
      <div class="col-sm-8">
        <a href="inner-sale.html"><img src="images/sale2.png" alt=""></a>
      </div>
    </div>


    <div class="row pack-zvon">
      <div class="zvonok2">
        <div class="row ">
          <div class="col-sm-6">
            <h3 class="zakaz-headers2">По всем вопросам звоните:</h3>
            <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
          </div>
          <div class="col-sm-6">
            <form id="zakaz-form" action="" method="post">
                <h3 class="zakaz-headers2">Или закажите обратный звонок:</h3>
                <input type="text" class="input-zvon2" placeholder="Введите свой телефон" name="">
                <input type="submit" id="zakazat2" name="zvonok" value="Заказать">
            </form>
          </div>
        </div>
      </div>
    </div>

  </main>
@endsection