<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;

class Department extends Model
{
    use CrudTrait;
    use Sluggable;
    use SearchableTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'departments';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['title', 'slug', 'image', 'content', 'active', 'icon', 'parent_id', 'lft', 'rgt', 'depth'];
    public $translatable = ['title', 'content'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_title',
            ],
        ];
    }

	public function getDepartmentMenuItems()
	{
		$items = $this->where('active', 1)->orderBy('lft')->whereNull('parent_id')->get(['title', 'slug', 'icon', 'image']);

        $menuItems = collect([]);
        foreach($items as $item)
        {
            if($item->slug == 'uzi') {
				$menuItems->push(['url' => route('department', ['depSlug' => $item->slug]), 'icon' => '/uploads/'.$item->icon, 'image' => '/uploads/'.$item->image, 'title' => $item->title]);
				$menuItems->push(['url' => route('checks') . '?ids=24', 'icon' => '/images/list.svg', 'title' => 'Check-up']);
			} else {
				$menuItems->push(['url' => route('department', ['depSlug' => $item->slug]), 'icon' => '/uploads/'.$item->icon, 'image' => '/uploads/'.$item->image, 'title' => $item->title]);
			}
        }
		return $menuItems;
	}

    protected $searchable = [
        'columns' => [
            'departments.title' => 10,
            'departments.content' => 7
        ]
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'parent_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getSlugOrTitleAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }

        return $this->title;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "uploads";
        $destination_path = "departments";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            $image->resize(1200, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            // 1. Generate a filename.
            $filename = md5($value.time()).'.png';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }
     public function setIconAttribute($value)
    {
        $attribute_name = "icon";
        $disk = "uploads";
        $destination_path = "department";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
