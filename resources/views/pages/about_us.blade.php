@extends('master')

@section('content')
<main>
    <div class="company">
		<div class="company-name">
			<h3>{{ $page->photo_name }}</h3>
			<p>{{ $page->photo_position }}</p>
		</div>
      <div class="company-info">
        <h1>{{ $page->block_1_title_1 }}</h1>
        <p>
          {!! $page->block_1_description_1 !!}
        </p>
        <h4>{{ $page->block_1_title_2 }}</h4>
        {!! $page->block_1_description_2 !!}
      </div>
    </div>
	<div class="new-history">
		<h3>История компании</h3>
		<div class="row">
			@foreach($histories as $key=>$history)
			<p>{{$history->title}}г. - {{ $history->content }}</p>
			@endforeach
		</div>
	</div>
    <div class="mission">
    <h3>{{ $page->block_2_title_1 }}</h3>
      <p>{!! $page->block_2_description_1 !!}</p>
    </div>
    <div class="container main-directions">
      <h3>Основные направления нашего центра:</h3>
      <div class="row">
        <div class="col-sm-4">
          <img src="/images/first-aid-kit.png" alt="">
          <p>Лечение</p>
        </div>
        <div class="col-sm-4">
          <img src="/images/microscope.png" alt="">
          <p>Диагностика</p>
        </div>
        <div class="col-sm-4">
          <img src="/images/graduated-student.png" alt="">
          <p>Образование</p>
        </div>
      </div>

    </div>
    <div class="history">

      <div class="history-line"><img src="images/Line 11.1.png" alt=""></div>
      <div class="container">
        <h3>Наша история </h3>


          @foreach($histories as $key=>$history)
          <hgroup class="bubble {{ ($key % 2 == 1) ? 'right' : 'left' }}-bub">
            <div class="his his-{{ ($key % 2 == 1) ? 'right' : 'left' }}">
              <h5>{{ $history->title }}</h5>
              <div class="content">
                {!! $history->content !!}
              </div>
            <img src="{{ asset('uploads/' . $history->image) }}" alt="{{ $history->title }}">
            </div>
          </hgroup>
          @endforeach



      </div>
    </div>



  </main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
<style>
  .his .content {
    color: #757575;
    padding: 10px 20px 10px 60px;
}
  .content ul {
    padding: 0;
}
</style>

@endsection
