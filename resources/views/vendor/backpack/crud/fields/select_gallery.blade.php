<!-- select from array -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <select
        onchange="selectType()" id="select" name="{{ $field['name'] }}@if (isset($field['allows_multiple']) && $field['allows_multiple']==true)[]@endif"
        @include('crud::inc.field_attributes')
        @if (isset($field['allows_multiple']) && $field['allows_multiple']==true)multiple @endif
        >

        @if (isset($field['allows_null']) && $field['allows_null']==true)
            <option value="">-</option>
        @endif

        @if (count($field['options']))
            @foreach ($field['options'] as $key => $value)
                @if((old($field['name']) && (
                        $key == old($field['name']) ||
                        (is_array(old($field['name'])) &&
                        in_array($key, old($field['name']))))) ||
                        (null === old($field['name']) &&
                            ((isset($field['value']) && (
                                        $key == $field['value'] || (
                                                is_array($field['value']) &&
                                                in_array($key, $field['value'])
                                                )
                                        )) ||
                                (isset($field['default']) &&
                                ($key == $field['default'] || (
                                                is_array($field['default']) &&
                                                in_array($key, $field['default'])
                                            )
                                        )
                                ))
                        ))
                    <option value="{{ $key }}" selected>{{ $value }}</option>
                @else
                    <option value="{{ $key }}">{{ $value }}</option>
                @endif
            @endforeach
        @endif
    </select>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

@push('crud_fields_scripts')
<script>
    function selectType() {

        var e = document.getElementById("select");
        var strUser = document.getElementById("select").options[document.getElementById("select").selectedIndex].value;
        console.log(e);

        if (strUser == 'image'){
            $('#select-image').show();
            $('#select-video').hide();
        } else if(strUser == 'video'){
            $('#select-image').hide();
            $('#select-video').show();
        }
    };

    $(document).ready(function () {
        var e = document.getElementById("select");
        var strUser = e.options[e.selectedIndex].value;

        if (strUser == 'image'){
            $('#select-image').show();
            $('#select-video').hide();
        } else if(strUser == 'video'){
            $('#select-image').hide();
            $('#select-video').show();
        }
    })

</script>
@endpush