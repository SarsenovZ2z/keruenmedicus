@extends('master') 

@section('content')
<main>
    <div class="gallery">

        <div class="container">
             <h1 class="gallery-photosHead">Фото-галерея </h1>
            <h1 class="gallery-videosHead">Видео-галерея </h1>
            
           
            @foreach($photos->chunk(3) as $photoChunk)
            <div class="galery--photos">
            <div class="row">
                @foreach($photoChunk as $photo)
                @if($photo->select == 'image')
                <div class="col-sm-4">
                    
                        <a href="{{ asset('uploads/' . $photo->image) }}" data-lightbox="lol"><img src="{{ asset('uploads/' . $photo->image) }}" alt="{{ $photo->title }}"></a>
                    
                    
                        <p>{{ $photo->title }}</p>

                </div>
                @endif    
                @endforeach
            </div>
        </div>
            @endforeach
            @foreach($videos->chunk(3) as $photoChunk)
            <div class="gallery-videos">
              <div class="row">
                @foreach($photoChunk as $photo)
                <div class="col-sm-4">
                  <iframe src="https://www.youtube.com/embed/{{ $photo->video['id'] }}" allowFullScreen>
                  </iframe>
                  <p>{{ $photo->title }}</p>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
    <div class="row pack-zvon">
        <div class="zvonok2">
            <div class="row ">
                <div class="col-sm-6">
                    <h3 class="zakaz-headers2">По всем вопросам звоните:</h3>
                    <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                </div>
                <div class="col-sm-6">
                    <form id="zakaz-form" action="" method="post">
                        <h3 class="zakaz-headers2">Или закажите обратный звонок:</h3>
                        <input type="text" class="input-zvon2" placeholder="Введите свой телефон" name="">
                        <input type="submit" id="zakazat2" name="zvonok" value="Заказать">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="1" class="album text-muted zapis">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="txxt1">Запишитесь на прием.</h2>
                </div>
                <div class="col-sm-6">
                    <p class="txxt2">Запишитесь на прием, заполнив данные. Мы свяжемся с Вами в самое ближайшее время и согласуем время
                        <span
                            class="mob-hide">или проконсультируем по всем интересующим Вас вопросам</span>.</p>
                </div>
            </div>
            <p class="required">* - обязательно для заполнения</p>
            <br>
            <form action="" method="post">
                <div class="row">
                    <div class="col-sm-4">
                        <p class="txxt">Имя</p>
                        <input class="basic-info" type="text">

                    </div>
                    <div class="col-sm-4">
                        <p class="txxt">Телефон
                            <span class="required">*</span>
                        </p>
                        <input class="basic-info" type="text">

                    </div>
                    <div class="col-sm-4 email">
                        <p class="txxt">Email</p>
                        <input class="basic-info" type="text">

                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="txxt">Направление
                            <span class="required">*</span>
                        </p>
                        <select class="sec-info">
                            <option></option>
                            <option></option>
                            <option></option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <p class="txxt">Дата приема</p>
                        <select class="sec-info">
                            <option></option>
                            <option></option>
                            <option></option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <p class="txxt" id="comment">Комментарий</p>
                    <textarea></textarea>


                </div>
                <div class="row both">
                    <div class="col-sm-6 files">
                        <input type="file" name="file" id="file" class="inputfile">
                        <label for="file">Выбрать</label>
                        <span id="label-text">Прикрепить файл</span>
                    </div>
                    <div class="col-sm-6 files">
                        <input type="submit" id="makeReg" value="Записаться">
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
<link rel="stylesheet" href="{{ asset('css/lightbox.css') }}">
@endsection

@section('after_jquery')
<script src="{{ asset('js/lightbox.js') }}"></script>

@endsection