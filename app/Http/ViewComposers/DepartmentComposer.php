<?php

namespace App\Http\ViewComposers;

use App\Models\Department;
use Illuminate\View\View;



class DepartmentComposer
{
    private $department;

    public function __construct(Department $items)
    {
        $this->department = $items;



    }
    public function compose(View $view)
    {
        $view->with('menuDepartments', $this->department->getDepartmentMenuItems());
    }
}