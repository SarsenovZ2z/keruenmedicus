@extends('master') 

@section('content')
<main role="main">
    <div class="container category-container news-1-1">
        <div class="row category-row">
            <div class="col-sm-6 desc">
                <div class="container">
                    <div class="album packets news-1">
                        <div class="row">
                            <div class="container packs-overlay">

                                <h1>{{ $post->title }}</h1>
                            <p class="small">{{ $post->created_at }}</p>

                                {!! $post->content !!}
                            </div>
                        <img src="{{ asset('uploads/' . $post->image) }}">
                        </div>
                    </div>
                <img class="del-line" width="100%" src="{{ asset('images/Line%209.png') }}">

                    <!--<div class="row social">
                        <div class="col-sm-6 heading">
                            <p>Поделиться с друзьями:</p>
                        </div>
                        <div class="col-sm-6 icons">
                            <div class="row">
                                <div class="col-sm-3">
                                    <a href="">
                                    <img src="{{ asset('images/facebook.png') }}">
                                    </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="">
                                        <img src="{{ asset('images/twitter.png') }}">
                                    </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="">
                                        <img src="{{ asset('images/vkontakte-logo.png') }}">
                                    </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="">
                                        <img src="{{ asset('images/odnoklassniki-logo.png') }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>-->

                    <div class="row ">
                        <div class="zvonok">
                            <h3 class="zakaz-headers">По всем вопросам звоните:</h3>
                            <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                            <form id="zakaz-form" action="{{ route('form.phone') }}" method="post">
                                    {{ csrf_field() }}
                                <h3 class="zakaz-headers">Или закажите обратный звонок:</h3>
                                <input type="text" class="input-zvon" placeholder="Введите свой телефон" name="phone">
                                <input type="submit" id="zakazat" name="zvonok" value="Заказать">
                            </form>
                        </div>
                    </div>

                    <!--<div class="row review comm">
                        <div class="container">
                            <div class="row">

                                <h2 class="">Оставить комментарий</h2>
                            </div>

                            <br>
                            <form class="forma-otzyv" action="" method="post">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input class="base-info" type="text" placeholder="Введите имя">

                                    </div>
                                    <div class="col-sm-6">
                                        <input class="base-info second" type="text" placeholder="Введите свой Email">

                                    </div>
                                </div>

                                <br>
                                <div class="row otzyv-attach">

                                    <textarea placeholder="Оставьте свой отзыв"></textarea>


                                </div>
                                <div class="row both">
                                    <div class="col-sm-6 files">
                                        <input type="file" name="file" id="file" class="inputfile">
                                        <label for="file">Выбрать</label>
                                        <div class="txt-label">
                                            <span id="label-text">Прикрепить файл</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 files">
                                        <input type="submit" id="makeReg" value="Оставить отзыв">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>-->

                </div>
            </div>


            <div class="col-sm-6 sidebar news-1-side">

                <div class="container">
					@foreach($news as $post)
                    <div class="row">
                        <a href="{{ route('post', ['postSlug' => $post->slug]) }}">
                            <p>{{ $post->title }}</p>
                        </a>
                        <p class="small text-muted">{{ $post->created_at }}</p>
                    </div>
					@endforeach
                    
                </div>

            </div>
        </div>
    </div>
</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">

@endsection