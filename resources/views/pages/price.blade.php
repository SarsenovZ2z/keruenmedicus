@extends('master')

@section('content')
<main>
  <div class="price">
    <div class="price__txt-wrp">
        <h3 class="price-list-heading">Прейскурант</h3>
        <p>
           Выберите из списка необходимую
          услугу. <a href="{{ asset('/uploads/'.Settings::get('price')) }}" download="Прайс">Вы можете
          скачать весь прейскурант. </a>
        </p>
      </div>
      <div class="price__inner">
        <nav class="price__nav-wrp">
          <div class="price__search__wrpwrp">
            <div class="price__search-wrp">
              <input type="text" placeholder="Поиск по ключевому слову" class="price__srch" id="searchPrice" name="search">
              <button class="price__srch-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26.086 26.086">
                  <defs>
                    <style>
                      .price-srch-1, .price-srch-3 {
                        fill: none;
                      }

                      .price-srch-1 {
                        stroke: #fff;
                        stroke-width: 2px;
                      }

                      .price-srch-2 {
                        stroke: none;
                      }
                    </style>
                  </defs>
                  <g transform="translate(-335.846 -503.173)">
                    <g class="price-srch-1" transform="translate(349.298 503.173) rotate(45)">
                      <circle class="price-srch-2" cx="8.933" cy="8.933" r="8.933"/>
                      <circle class="price-srch-3" cx="8.933" cy="8.933" r="7.933"/>
                    </g>
                    <path class="price-srch-1" d="M0,0V9.091" transform="translate(342.981 522.124) rotate(45)"/>
                  </g>
                </svg>
              </button>
            </div>
          </div>
          <ul class="price__list" id="firstPrice">
			@foreach($prices as $key=>$price)
            <li class="price__item" data-index="{{ $key }}">{{ $price->title }}</li>
		    @endforeach
          </ul>
          <div class="price__more">
            Еще
          </div>
        </nav>
        <div class="price__content" id="secondPrice">
		  @foreach($prices as $key=>$price)
          <div class="price__tab" data-index="{{ $key }}">
            <div class="price__row">
              <div class="price__col">
                <span class="price__heading price__heading--name">Терапевтический профиль</span>
              </div>
              <div class="price__col">
                <span class="price__heading">Процедура</span>
              </div>
              <div class="price__col">
                <span class="price__heading">Цена</span>
              </div>
            </div>
			@if(isset($price->price))
			@foreach(json_decode($price->price) as $item)

            <div class="price__row">
              <div class="price__col">
                <span class="price__service">{{ (isset($item->name)) ? $item->name : '' }}</span>
              </div>
              <div class="price__col">
                <span class="price__procedure">{{ (isset($item->procedure)) ? $item->procedure : '' }}</span>
              </div>
              <div class="price__col">
                <span class="price__cost">{{ (isset($item->price)) ? $item->price : '' }}</span>
              </div>
            </div>
			@endforeach
			@endif
          </div>
		  @endforeach
        </div>
        <div class="price__content-mobile" id="thirdPrice">
		  @foreach($prices as $key=>$price)
          <div class="price__tab" data-index="{{ $key }}">
            <div class="price__header">
                <button class="price__bck-icon">
                <img src="/images/prev.svg" alt="back">
                </button>
                <h5 class="price__head">{{ $price->title }}</h5>
            </div>
			@if(isset($price->price))
			@foreach(json_decode($price->price) as $item)
            <div class="price__row-mobile">
              <p class="price__name">{{ (isset($item->name)) ? $item->name : '' }}</p>
              <div>
              <span class="price__procedure-mobile">{{ (isset($item->procedure)) ? $item->procedure : '' }}</span>
              <span class="price__cost-mobile">{{ (isset($item->price)) ? $item->price : '' }}</span>
              </div>
            </div>
            @endforeach
			@endif
          </div>
		  @endforeach
        </div>
      </div>
      </div>
    {{-- <a class="price-download" href="/pdf/price.xlsx" target="_blank">Скачать прайс</a>
      <div class="priceWrap">
       <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="/pdf/price.pdf" allowfullscreen></iframe>
        </div>
      </div> --}}
</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
@endsection

@section('after_jquery')
<script type="text/javascript">

  $('#searchPrice').on('keyup',function(){

  $value=$(this).val();

  $.ajax({

  type : 'get',

  url : '{{route('search.price')}}',

  data:{'search':$value},

  success:function(data){
   console.log(data);
  $('#firstPrice').html(data.first);
  $('#secondPrice').html(data.second);
  $('#thirdPrice').html(data.third);

  }

  });



  })

  </script>

  <script type="text/javascript">

  $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });

  </script>
@endsection
