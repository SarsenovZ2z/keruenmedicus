<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['pages.home', 'pages.news.news', 'pages.news.index', 'pages.sale.sales', 'pages.sale.index', 'pages.about_us', 'pages.checkup', 'pages.contact', 'pages.department', 'pages.doctor', 'pages.doctors', 'pages.education', 'pages.equipment', 'pages.gallery', 'pages.price', 'pages.package', 'pages.packages', 'pages.search', 'pages.home-new', 'pages.history', 'pages.carier'] , 'App\Http\ViewComposers\DepartmentComposer');
        
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
