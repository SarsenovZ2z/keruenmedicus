<?php

namespace App;

trait PageTemplates
{
    /*
    |--------------------------------------------------------------------------
    | Page Templates for Backpack\PageManager
    |--------------------------------------------------------------------------
    |
    | Each page template has its own method, that define what fields should show up using the Backpack\CRUD API.
    | Use snake_case for naming and PageManager will make sure it looks pretty in the create/update form
    | template dropdown.
    |
    | Any fields defined here will show up after the standard page fields:
    | - select template
    | - page name (only seen by admins)
    | - page title
    | - page slug
    */

    private function home()
    {
        $this->crud->addField([   // CustomHTML
                        'name' => 'metas_separator',
                        'type' => 'custom_html',
                        'value' => '<br><h2>SEO</h2><hr>',
                    ]);
        $this->crud->addField([
                        'name' => 'meta_title',
                        'label' => 'SEO - title',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([
                        'name' => 'meta_description',
                        'label' => 'SEO - description',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([   // CustomHTML
                        'name' => 'content_separator_1',
                        'type' => 'custom_html',
                        'value' => '<br><h2>Редактировать зеленый блок</h2><hr>',
                    ]);
        // 1-блок
        $this->crud->addField([
                        'name' => 'block_1_title_1',
                        'label' => 'Заголовок №1',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([
                        'name' => 'block_1_heading_1',
                        'label' => 'Подзаголовок №1',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        // 2-блок
        $this->crud->addField([
                        'name' => 'block_1_title_2',
                        'label' => 'Заголовок №2',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([
                        'name' => 'block_1_heading_2',
                        'label' => 'Подзаголовок №2',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        // 3-блок
        $this->crud->addField([
                        'name' => 'block_1_title_3',
                        'label' => 'Заголовок №3',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([
                        'name' => 'block_1_heading_3',
                        'label' => 'Подзаголовок №3',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        // 4-блок
        $this->crud->addField([
                        'name' => 'block_1_title_4',
                        'label' => 'Заголовок №4',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([
                        'name' => 'block_1_heading_4',
                        'label' => 'Подзаголовок №4',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);

        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_2',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать синий блок</h2><hr>',
        ]);
        // 1-блок
        $this->crud->addField([
                        'name' => 'block_2_title_1',
                        'label' => 'Заголовок',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([
                        'name' => 'block_2_description_1',
                        'label' => 'Описание',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_3',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать белый блок</h2><hr>',
        ]);
        // 1-блок
        $this->crud->addField([
                        'name' => 'block_3_title_1',
                        'label' => 'Заголовок',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
        $this->crud->addField([
                        'name' => 'block_3_description_1',
                        'label' => 'Описание',
                        'fake' => true,
                        'store_in' => 'extras',
                    ]);
    }

    private function about_us()
    {
        $this->crud->addField([   // CustomHTML
                    'name' => 'metas_separator',
                    'type' => 'custom_html',
                    'value' => '<br><h2>SEO</h2><hr>',
        ]);
        $this->crud->addField([
                    'name' => 'meta_title',
                    'label' => 'SEO - title',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([
                    'name' => 'meta_description',
                    'label' => 'SEO - description',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_1',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать первый блок</h2><hr>',
        ]);

        $this->crud->addField([
                    'name' => 'photo_name',
                    'label' => 'ФИО',
                    'fake' => true,
                    'store_in' => 'extras',
                ]);

        $this->crud->addField([
                    'name' => 'photo_position',
                    'label' => 'Должность',
                    'fake' => true,
                    'store_in' => 'extras',
                ]);
        // 1-блок
        $this->crud->addField([
                    'name' => 'block_1_title_1',
                    'label' => 'Заголовок №1',
                    'fake' => true,
                    'store_in' => 'extras',
                ]);
        $this->crud->addField([
                    'name' => 'block_1_description_1',
                    'label' => 'Описание 1-заголовка',
                    'fake' => true,
                    'store_in' => 'extras',
                    'type' => 'summernote'
                ]);
        $this->crud->addField([
                    'name' => 'block_1_title_2',
                    'label' => 'Заголовок №2',
                    'fake' => true,
                    'store_in' => 'extras',
                ]);
        $this->crud->addField([
                    'name' => 'block_1_description_2',
                    'label' => 'Описание 2-заголовка',
                    'fake' => true,
                    'store_in' => 'extras',
                    'type' => 'summernote'
                ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_2',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать синий блок</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'block_2_title_1',
            'label' => 'Заголовок №1',
            'fake' => true,
            'store_in' => 'extras',
        ]);
$this->crud->addField([
            'name' => 'block_2_description_1',
            'label' => 'Описание',
            'fake' => true,
            'store_in' => 'extras',
        ]);
    }

    private function equipment()
    {
        $this->crud->addField([   // CustomHTML
            'name' => 'metas_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>SEO</h2><hr>',
        ]);
        $this->crud->addField([
                    'name' => 'meta_title',
                    'label' => 'SEO - title',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([
                    'name' => 'meta_description',
                    'label' => 'SEO - description',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_1',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать первый блок</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'block_1_title_1',
            'label' => 'Заголовок №1',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'block_1_description_1',
            'label' => 'Описание 1-заголовка',
            'fake' => true,
            'store_in' => 'extras',
            'type'  => 'textarea'
        ]);
    }

    private function education()
    {
        $this->crud->addField([   // CustomHTML
            'name' => 'metas_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>SEO</h2><hr>',
        ]);
        $this->crud->addField([
                    'name' => 'meta_title',
                    'label' => 'SEO - title',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([
                    'name' => 'meta_description',
                    'label' => 'SEO - description',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_1',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать первый блок</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'block_1_title_1',
            'label' => 'Заголовок №1',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'block_1_description_1',
            'label' => 'Описание №1',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'block_1_description_2',
            'label' => 'Описание №2',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'block_1_description_3',
            'label' => 'Описание №3',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);
    }

    private function contact()
    {
        $this->crud->addField([   // CustomHTML
            'name' => 'metas_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>SEO</h2><hr>',
        ]);
        $this->crud->addField([
                    'name' => 'meta_title',
                    'label' => 'SEO - title',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([
                    'name' => 'meta_description',
                    'label' => 'SEO - description',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_1',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать страницу</h2><hr>',
        ]);

        $this->crud->addField([
            'name' => 'address',
            'label' => 'Адрес',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'summernote'
        ]);
        $this->crud->addField([
            'name' => 'phones',
            'label' => 'Телефонные номера',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'summernote'
        ]);
        $this->crud->addField([
            'name' => 'email',
            'label' => 'e-mail адрес',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'summernote'
        ]);
    }

    private function carier()
    {
        $this->crud->addField([   // CustomHTML
            'name' => 'metas_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>SEO</h2><hr>',
        ]);
        $this->crud->addField([
                    'name' => 'meta_title',
                    'label' => 'SEO - title',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([
                    'name' => 'meta_description',
                    'label' => 'SEO - description',
                    'fake' => true,
                    'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator_1',
            'type' => 'custom_html',
            'value' => '<br><h2>Редактировать страницу</h2><hr>',
        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => 'Описание страницы',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'content1_heading',
            'label' => 'контент 1 (Заголовок)',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'content1',
            'label' => 'контент 1',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'summernote'
        ]);
        $this->crud->addField([
            'name' => 'content2_heading',
            'label' => 'контент 2 (Заголовок)',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'content2',
            'label' => 'контент 2',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'summernote'
        ]);
        $this->crud->addField([
            'name' => 'content2_emails',
            'label' => 'Контактные адреса',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'table',
            'entity_singular' => 'option', // used on the "Add X" button
            'columns' => [
                'email' => 'Email',
            ],
        ]);

        $this->crud->addField([
            'name' => 'content3_heading',
            'label' => 'контент 3 (Заголовок)',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'content3',
            'label' => 'контент 3',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'table',
            'entity_singular' => 'option', // used on the "Add X" button
            'columns' => [
                'stage' => 'Этап',
                'content' => 'Описание',
            ],
        ]);
        $this->crud->addField([
            'name' => 'content4_heading',
            'label' => 'контент 4 (Заголовок)',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'content4',
            'label' => 'контент 4',
            'fake' => true,
            'store_in' => 'extras',
            'type' => 'summernote'
        ]);
    }
}
