@extends('master') 

@section('content')
<main>
	<div class="slider-cat">
		
        <img class="big-img" src="/images/5.jpg">
        <div class="cat-overlay"></div>
        <img class="mob-img" src="/images/5.jpg">
		
    <h1 class="slider-header">Check-up</h1>
    </div>
    <div class="department">
        <div class="container">
			<div class="one-times">
            @foreach($menuDepartments->chunk(5) as $items)
            <div class="row">
                @foreach($items as $item)
                <a class="dep-link" href="{{ $item['url'] }}">
                    <div class="col-sm-2">
                        <div class="depWrap">
                        <img src="{{ $item['icon'] }}" alt="{{ $item['title'] }}">
                        </div>
                        <p class="dep-name">{{ $item['title'] }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            @endforeach
			</div>
        </div>
    </div>
	<div class="sliderMobile">
        <h2 class="kind">Направление</h2>
        <div class="one-time">
			@foreach($menuDepartments as $key=>$dep)
          	<div>
            	<a href="{{ $dep['url'] }}">
              		<div class="overlay">
                		<img id="img{{$key}}" src="{{ $dep['icon'] }}" alt="{{ $dep['title'] }}">
              		</div>
              		<p class="card-text1">{{ $dep['title'] }}</p>
            	</a> 
          	</div>
			@endforeach
        </div>
      </div>
    <div class="container checkup">
        <h4 class="checkup-head">Check-up пакеты для Вас и Ваших близких</h4>
        <p class="checkup-txt">Комплексное диагностическое обследование минимизирует влияние факторов риска и позволяет обнаружить болезнь на самой
            ранней стадии развития, когда она еще не причинила значительного вреда организму.</p>
        <div class="row">
            <div class="col-sm-6">
                <ul class="checkup-list">
                    @foreach($checks as $item)
                    <li class="checkup-item">
                    <a data-ref="{{ $item->id }}" href="">{{$item->title}}</a>
                        <span class="check-arrow">
                        <img src="{{ asset('images/downs.png') }}" alt="">
                        </span>
                        <div class="check-mob">
                            <h5 class="checkup-name">{{$item->title}}</h5>
                            {!! $item->content !!}
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-6">
                @foreach($checks as $item)
                <div id="{{$item->id}}" class="checkup-type">
                <h5 class="checkup-name">{{ $item->title }}</h5>
                    {!! $item->content !!}
                </div>
                @endforeach
            </div>
        </div>
    </div>

<img class="del-line2" width="750.84" src="{{ asset('images/Line%209.png') }}">
    <div class="row social news-soc">
        <div class="col-sm-6 heading2">
            <p>Поделиться с друзьями:</p>
        </div>
        <div class="col-sm-6 icons2">
            <div class="row">
                <div class="col-sm-3">
                    <a href="">
                    <img src="{{ asset('images/facebook.png') }}">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="{{ asset('images/twitter.png') }}">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="{{ asset('images/vkontakte-logo.png') }}">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="{{ asset('images/odnoklassniki-logo.png') }}">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row pack-zvon">
        <div class="zvonok2">
            <div class="row ">
                <div class="col-sm-6">
                    <h3 class="zakaz-headers2">По всем вопросам звоните:</h3>
                    <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                </div>
                <div class="col-sm-6">
                        <form id="zakaz-form" action="{{ route('form.phone') }}" method="post">
                                {{ csrf_field() }}
                        <h3 class="zakaz-headers2">Или закажите обратный звонок:</h3>
                        <input type="text" class="input-zvon2" placeholder="Введите свой телефон" name="phone">
                        <input type="submit" id="zakazat2" name="zvonok" value="Заказать">
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
@endsection

@section('after_jquery')
<script>
$(document).ready(function () {
  var listItems = $('.checkup-item a');
  console.log(window.location.href);
  $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
  }
  
  for ( ind = 0, len = listItems.length; ind < len; ind++ ) {
      product = $(listItems[ind]);
      var f = $(product).data('ref');
      if($(product).data('ref') == $.urlParam('ids')){
        console.log('succeded');
        if(window.outerWidth > 800){
          $('.checkup-item').removeClass('checkup-item--active');
          $(product).parent().addClass('checkup-item--active');
        }
        var id = "#" + $.urlParam('ids');
        console.log(id);
        // console.log($(id).find("h3").text());
        $(id).addClass('checkup-type--active');
        // $('.checkup-type').text(txt);
      }
      else{
        console.log('failed');
      }
      // ...
  }
  $('.MsoNormalTable').parent().addClass('over');
});
</script>	
	
@endsection