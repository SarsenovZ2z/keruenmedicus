@extends('master') 

@section('content')
<main role="main">
    <div class="container news">
        <h1 class="news-heading">Новости</h1>
        @foreach($news->chunk(4) as $items)
        <div class="row news-rows">
            @foreach($items as $item)
            <div class="col-sm-3 @if($loop->last) last @endif">
                <a href="{{ route('post', ['postSlug' => $item->slug]) }}">
                    <img src="{{ asset('uploads/' . $item->image) }}">
                </a>
                <div class="caption">
                <p class="small text-muted">{{ $item->created_at }}</p>
                <a href="{{ route('post', ['postSlug' => $item->slug]) }}">
                        <p class="medium">{{ $item->title }}</p>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
        <div class="pagination">
   
			{{ $news->links('partials.pagination') }}


        
            {{-- <a href="#">
                <img src="images/Group%2030.png">
            </a>
            <a href="#">1</a>
            <a class="active" href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">
                <img src="images/Group%2032.png">
            </a> --}}
        </div>
    </div>


    <img class="del-line2" width="750.84" src="images/Line%209.png">

    {{-- <div class="row social news-soc">
        <div class="col-sm-6 heading2">
            <p>Поделиться с друзьями:</p>
        </div>
        <div class="col-sm-6 icons2">
            <div class="row">
                <div class="col-sm-3">
                    <a href="">
                        <img src="images/facebook.png">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="images/twitter.png">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="images/vkontakte-logo.png">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="images/odnoklassniki-logo.png">
                    </a>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row news-zvon">
        <div class="zvonok2">
            <div class="row ">
                <div class="col-sm-6">
                    <h3 class="zakaz-headers2">По всем вопросам звоните:</h3>
                    <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                </div>
                <div class="col-sm-6">
                        <form id="zakaz-form" action="{{ route('form.phone') }}" method="post">
                                {{ csrf_field() }}
                        <h3 class="zakaz-headers2">Или закажите обратный звонок:</h3>
                        <input type="text" class="input-zvon2" placeholder="Введите свой телефон" name="phone">
                        <input type="submit" id="zakazat2" name="zvonok" value="Заказать">
                    </form>
                </div>
            </div>
        </div>
    </div>

</main>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">

@endsection