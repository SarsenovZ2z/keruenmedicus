@extends('master')

@section('content')
<main role="main">
      <div class="search">
        <div class="searchData">
        <p>Данные, соответствующие критериям поиска</p> <p class="searchName">{{ isset($q) ? $q : '' }}</p>
      </div>
        <form class="searchForm" action="{{ route('search') }}" method="GET">
          <input type="text" class="searchInput" name="q" id="">
          <input type="submit" class="searchBtn" name="" id="">
        </form>
        <div class="searchList">
          @if(count($news) > 0)
          <h5>Новости</h5>
          @foreach($news as $post)
            <a class="searchItem" href="{{ route('post', ['postSlug' => $post->slug]) }}">{{ $post->title }}</a>
          @endforeach
          @endif

          @if(count($departments) > 0)
          <h5>Направления</h5>
          @foreach($departments as $dep)
            <a class="searchItem" href="{{ route('department', ['depSlug' => $dep->slug]) }}">{{ $dep->title }}</a>
          @endforeach
          @endif

          @if(count($doctors) > 0)
          <h5>Врачи</h5>
          @foreach($doctors as $doc)
            <a class="searchItem" href="{{ route('doctor', ['docSlug' => $doc->slug]) }}">{{ $doc->title }}</a>
          @endforeach
          @endif

          @if(count($checks) > 0)
          <h5>Чек-апы</h5>
          @foreach($checks as $check)
            <a class="searchItem" href="{{ route('checks', ['ids' => $check->id]) }}">{{ $check->title }}</a>
          @endforeach
          @endif

          @if(count($packages) > 0)
          <h5>Пакеты</h5>
          @foreach($packages as $package)
            <a class="searchItem" href="{{ route('package', ['packSlug' => $package->slug]) }}">{{ $package->title }}</a>
          @endforeach
          @endif

          @if(count($prices) > 0)
          <h5>Прейскурант</h5>
          @foreach($prices as $price)
            <a class="searchItem" href="{{ route('price', ['active' => $price->id]) }}">{{ $price->title }}</a>
          @endforeach
          @endif
          
        </div>
      </div>
    </main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">

@endsection