$(".eq-types ul .equipment-list").click(function () {
  $(this).toggleClass("list");
  $(this).find(".hidden-eq").toggleClass("show");
});
$(".dropdown").click(function () {
  $(".dropdown").removeClass('drop');
  $(".dropdown-menu").removeClass("show");
  $(this).toggleClass("drop");
  var data = $(".drop a").data('href');
  $(data).toggleClass("show");

});
$(".price-list li").click(function () {
  $(this).toggleClass("list-price");
  $(this).find(".hidden-price").toggleClass("show");
});

$(".drop-item").click(function () {
  $(this).toggleClass("dropped");
  $(this).find(".plus").toggleClass("minus");
  $(this).find(".hidden-menu").toggleClass("sezam");

});

$(".category h3").click(function () {
  $(this).parent().toggleClass("drop-doctor");
});

$(document).ready(function () {
  // if(window.outerWidth > 600){
  // 	$(".checkup-inner img").hover(function(){
  // 		$(this).parent().toggleClass("show-info");
  // 	});
  // }
  // else{
  // 	$(".checkup-inner img").click(function(){
  // 		$(this).parent().toggleClass("show-info");
  // 	});
  // }
  if (window.outerWidth < 750) {
    $('.checkup-item').removeClass('checkup-item--active');
  }
});

$('.checkup-item a').click(function (e) {
  e.preventDefault();
  $('.checkup-item').removeClass('checkup-item--active');
  $(this).parent().addClass('checkup-item--active');
  if (window.outerWidth > 750) {
    $('.checkup-type').removeClass('checkup-type--active');
    var id = '#' + $(this).data('ref');
    $(id).addClass('checkup-type--active');
  }
  else {
    $('.check-mob').removeClass('check-show');
    $(this).parent().find('.check-mob').addClass('check-show');
  }
});
$('.category-item a').click(function (e) {
  e.preventDefault();
  $('.category-item').removeClass('category-item--active');
  $('.all-doctors').removeClass('all-doctors--active');
  $(this).parent().addClass('category-item--active');
  var id = '#' + $(this).data('ref');
  $(id).addClass('all-doctors--active');
});

if (window.outerWidth > 1024) {
  $('.search-btn--open').click(function (e) {
    e.stopPropagation();
    $(this).toggleClass('work');
    $('.search-input').toggleClass('anim');
  });
}
$(document).mouseup(function (e) {
  if ($(e.target).is('.anim') == false && $(e.target).is('.search-btn--open') == false) {
    $('.search-btn--open').removeClass('work');
    $('.search-input').removeClass('anim');

  }
});
$('.drop').hover(function () {
  var id = $(this).find('a').data('ref');
  console.log(id);
  $(id).toggleClass('opens');
});
$('.burger').click(function () {
  $(this).toggleClass('change');
  $(this).parent().toggleClass('pos');
  $('.all-container').toggleClass('shows');
});

$('.cat-item').click(function () {
  $(this).toggleClass('item-change');
  $(this).find('.docs-mob').toggleClass('doc-show');
  // $('.all-container').toggleClass('shows');
});

$('.price__more').click(function () {
  $(this).text(function (i, text) {
    return text === "Скрыть" ? "Еще" : "Скрыть";
  });
  $('.price__list').toggleClass('price__list--open');
});
//if ($(window).width() > 601) {
$(document).ready(function () {
  var tab = $('.price__tab'),
    button = $('.price__item'),
    clickBtn,
    data,
    dataText;
  button.on('click', function () {
    tab.hide();
    data = $(this).data('index');
    clickBtn = $(this);
    dataText = $(this).text();
    button.removeClass('price__item--current');
    tab.each(function () {
      if (data == $(this).data('index')) {
        $(this).fadeIn(400);
        $(this).find('.price__heading--name').text(dataText);
        clickBtn.addClass('price__item--current');
      }
    });
  });
});
//}

if ($(window).width() < 600) {
  $(".price__item").click(function () {
    $(".price__nav-wrp").hide();
    $(".price__content-mobile").css('display', 'block');
  });
  $(".price__bck-icon").click(function () {
    $(".price__content-mobile").css('display', 'none');
    $(".price__nav-wrp").show();
  });
}

$(".vacancy__proff").click(function () {
  $(this).parent().find(".vacancy__desc").slideToggle(600);
  $(this).toggleClass("vacancy__proff--color");
  $(this).find(".vacancy__arrow").toggleClass("vacancy__arrow--color");
});
