
$('.navbar-toggle').click(function(){
    $('.navbar-toggle').toggleClass('opener');
    $('.navbar-nav').toggleClass('openMob');
});
$('.department .slick-dots li').click(function(){
  localStorage['slide']=$('.department .slick-current.slick-active').data('slick-index');
});

$('.categories-item').click(function(){
	localStorage['slide']=0;
});

$('.naprav .slick-dots li').click(function(){
	localStorage['slide']=$('.naprav .slick-current.slick-active').data('slick-index');
});

$(document).ready(function(){
	if(localStorage['slide'] !== null){
		$(".department .slick-slider").slick("goTo", localStorage['slide']);
	}
	if(window.outerWidth > 800){
		$('.dropdown').hover(function(){
			$(this).toggleClass('drop');
			$(this).find('.dropdown-menu').toggleClass('drop-menu');
		});
	}
	if(window.outerWidth < 800 && window.outerWidth > 500){
		$('.dropdown').click(function(){
			$(this).toggleClass('drop');
			$(this).find('.dropdown-menu').toggleClass('drop-menu');
		});
	}
	if(window.outerWidth < 500){
		$('.dropdown').click(function(){
			
			$(this).toggleClass('drop');
			$(this).find('.dropdown-menu').toggleClass('drop-menu');
		});
	}

	// Get the modal
	var modal = document.getElementById('myModal');
	var modal2 = document.getElementById('myModal2');

	// Get the button that opens the modal
	var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];
	var span2 = document.getElementsByClassName("close2")[0];

	// When the user clicks the button, open the modal 
	$('.header-order').click(function() {
	    modal.style.display = "block";
	    $('.all-container').removeClass('shows');
	    $('.burgerWrap').removeClass('pos');
	    $('.burger').removeClass('change');
	});
	$('.orderSend').click(function(e) {
		e.preventDefault();
	    modal2.style.display = "block";
	    setTimeout(TimeOut, 3000);
	});

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	    modal.style.display = "none";
	}
	
	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	    else if (event.target == modal2) {
	        modal2.style.display = "none";

	    }
	}
	span2.onclick = function() {
	    modal2.style.display = "none";
	}
	function TimeOut() {
		modal2.style.display = "none";
	}


});

$('.footer-blocks1 li').click(function(){
  	$(this).toggleClass('openerFootMob');
    $(this).find('.collapse').toggleClass('openFootMob');
});

$('.call-us').click(function(){
	$('.orderWithWrap').toggleClass('orderWithWrap--show');
});
$('.close3').click(function(){
	$('.orderWithWrap').toggleClass('orderWithWrap--show');
});




$(document).ready(function () {
  var burger = $('.mob-header__checkbox'),
    listItem = $('.mob-header__item'),
    nav = $('.mob-header__nav'),
    label = $('.mob-header__label'),
    background = $('.mob-header__background');

  label.click(function () {
    background.toggleClass('mob-header__background--open');
    label.toggleClass('mob-header__label--open');
    nav.toggleClass('mob-header__nav--open');
    console.log(listItem);
    if (background.hasClass('mob-header__background--open')) {
      TweenMax.staggerTo(listItem, 0.4, {
        y: 10,
        autoAlpha: 1,
        delay: 0.2
      }, 0.2)
    } else {
      TweenMax.to(listItem, 0.4, {
        y: -10,
        autoAlpha: 0
      })
    }
  })
  $('.mob-header__item--under').click(function () {
    $(this).find(".mob-header__drop").slideToggle(300);
  });
});

$('.gallery-photosHead').click(function () {
  $(this).parent().find(".gallery-videos").toggleClass("close");
  $(".galery--photos").removeClass("close");
});

$('.gallery-videosHead').click(function () {
 $(this).parent().find(".galery--photos").toggleClass("close");
 $(".gallery-videos").removeClass("close");
});

if ($(window).width() < 602) {

	window.onscroll = function() {myFunction()};

	var header = document.getElementById("myHeader");
	var sticky = header.offsetTop;
	var mob = document.getElementsByClassName("mob-header__label");

	function myFunction() {
	  if (window.pageYOffset > sticky) {
		header.classList.add("sticky");
		$(".mob-header__label").addClass("sticky2");
	  } else {
		header.classList.remove("sticky");
		  $(".mob-header__label").removeClass("sticky2");
	  }
	}
}

