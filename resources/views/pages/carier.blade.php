@extends('master')

@section('content')
<main>
  <div class="carier">
    <div class="carier__bck">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h5 class="carier__head">{{ $page->title }}</h5>
            <p class="carier__text">{{ $data->description }}</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row carier__gray carier__gray--img">
      <div class="col-sm-6">
        <img src="/images/proff.png" alt="pic" class="carier__pic">
      </div>
      <div class="col-sm-6">
        <h5 class="carier__head">{{ $data->content1_heading }}</h5>
        {!! $data->content1 !!}
      </div>
    </div>
    <div class="carier__white">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h5 class="carier__head">{{ $data->content2_heading }}</h5>
            {!! $data->content2 !!}
          </div>
          <div class="col-sm-6 carier__colLink">
              @foreach (json_decode($data->content2_emails) as $item)
                <a href="mailto:{{ $item->email }}" class="carier__link">{{ $item->email }}</a>
              @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="carier__gray">
      <div class="container">
        <h5 class="carier__head">{{ $data->content3_heading }}</h5>
        @foreach (json_decode($data->content3) as $item)
            <div class="row carier__stepRow">
              <div class="col-sm-4 carier__step">
                <h2 class="carier__name">{{ $item->stage }}</h2>
              </div>
              <div class="col-sm-7">
                <p class="carier__text carier__text--step">{!! $item->content !!}</p>
              </div>
            </div>
        @endforeach
      </div>
    </div>
  </div>
  @if(count($vacancies) > 0)
  <div class="container vacancy">
    <h5 class="carier__head">{{ $data->content4_heading }}</h5>
    <p class="carier__text">{!! $data->content4 !!}</p>
    <div class="vacancy__proffesions">
        @foreach($vacancies as $vacancy)
          <div class="vacancy__div">
            <div class="vacancy__proff">
              <div class="vacancy__info">
                <p class="vacancy__gray">{{ __('content.Должность') }}</p>
                <p class="vacancy__black">{{ $vacancy->title }}</p>
              </div>
              <div class="vacancy__info">
                <p class="vacancy__gray">{{ __('content.Опыт работы') }}</p>
                <p class="vacancy__black">{{ $vacancy->experience }}</p>
              </div>
              <div class="vacancy__info">
                <p class="vacancy__gray">{{ __('content.Зарплата') }}</p>
                <p class="vacancy__black">{{ $vacancy->salary }}</p>
              </div>
              <div class="vacancy__arrow"></div>
              <button class="vacancy__mobButton">{{ __('content.Подробнее') }}</button>
            </div>
            <div class="vacancy__desc">
                @foreach($vacancy->extras as $key=>$items)
                  <h5 class="vacancy__title">{{ __('content.'.$key) }}:</h5>
                  <ul class="vacancy__list">
                    @foreach(json_decode($items) as $item)
                        <li><p class="vacancy__text">{{ $item->content }}</p></li>
                    @endforeach
                  </ul>
                @endforeach
                <button type="submit" class="vacancy__button">{{ __('content.Откликнуться') }}</button>
                <div class="vacancy__attach">
                  <input type="file">
                  <p>{{ __('content.Прикрепить файл') }}</p>
                </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    @endif
  </div>
</main>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
@endsection
