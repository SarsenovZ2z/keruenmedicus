@extends('master') 


@section('content')
<main>
    <div class="one-doctor">
        <div class="row">
            <div class="col-sm-4">
            <img src="{{ asset('uploads/' . $doctor->image) }}" alt="{{ $doctor->title }}">
            </div>
            <div class="col-sm-8">
                <h3>{{ $doctor->title }}</h3>
                <h5>Опыт работы</h5>
                <p>{!! $doctor->experience !!}</p>
                <h5>Описание</h5>
                <div class="desc-txt">
                    {!! $doctor->content !!}
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">

@endsection