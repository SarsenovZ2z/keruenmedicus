@extends('master') 

@section('content')
<main role="main">

    <div class="album text-muted packets pack-page">
        <div class="container ">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="white-txt pack-head-txt">Пакеты медицинских услуг</h2>
                </div>
                <div class="col-sm-6">
                    <p class="white-txt pack-parag-txt">Приобретая программу, Вы экономите свое время и деньги По своей сути рыбатекст является альтернативой традиционному
                        lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.</p>
                </div>
            </div>
        </div>
        <div class="container packs-overlay">
            @foreach($packages->chunk(2) as $items)
            <br>
            <div class="row">
                @foreach($items as $key=>$item)
                <div class="col-sm-6 pack-item">
                <a href="{{ route('package', ['packSlug' => $item->slug]) }}">
                <img class="image-packets" id="image{{ $key }}" src="{{ isset($item->thumbnail) ? asset('uploads/' .  $item->thumbnail) :  asset('uploads/' .  $item->image)}}" alt="{{ $item->title }}">
                        <div class="middle">

                        <img class="cont" src="{{ asset('images/Arrow_big.png') }}">
                        </div>
                    <p class="card-text pack-def">{{ $item->title }}</p>
                    </a>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
    <img class="del-line2" width="750.84" src="{{ asset('images/Line%209.png') }}">



    <div class="row social pac-soc">
        <div class="col-sm-6 heading2">
            <p>Поделиться с друзьями:</p>
        </div>
        <div class="col-sm-6 icons2">
            <div class="row">
                <div class="col-sm-3">
                    <a href="">
                        <img src="/images/facebook.png">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="/images/twitter.png">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="/images/vkontakte-logo.png">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <img src="/images/odnoklassniki-logo.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row pack-zvon">
        <div class="zvonok2">
            <div class="row ">
                <div class="col-sm-6">
                    <h3 class="zakaz-headers2">По всем вопросам звоните:</h3>
                    <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                </div>
                <div class="col-sm-6">
                        <form id="zakaz-form" action="{{ route('form.phone') }}" method="post">
                                {{ csrf_field() }}
                        <h3 class="zakaz-headers2">Или закажите обратный звонок:</h3>
                        <input type="text" class="input-zvon2" placeholder="Введите свой телефон" name="phone">
                        <input type="submit" id="zakazat2" name="zvonok" value="Заказать">
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</main>
@endsection @section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}"> @endsection