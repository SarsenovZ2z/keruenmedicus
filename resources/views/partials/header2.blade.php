<header id="header" class="header">
  <div class="header__row">
    <div class="header__formWrapper">
      <form class="header__form" action="{{ route('search') }}" method="get">
        <input class="header__input" type="text" name="q" id="" placeholder="Поиск…">
        <input class="header__send" type="submit" name="" id="" style="border: none; cursor: pointer;">
      </form>
    </div>
    <div class="header__telWrapper">
      <a class="header__telLink" href="tel:{!! Settings::get('header_phone') !!}">
        <div class="header__tel">
          <span>{!! Settings::get('header_phone') !!}</span>
          <!-- <img class="header__arrow" src="/images/arrow.svg" alt=""> -->
        </div>
      </a>
      <a class="header__telLink" href="tel:{!! Settings::get('header_phone_2') !!}">
        <div class="header__tel">
          <span>{!! Settings::get('header_phone_2') !!}</span>
          <!-- <img class="header__arrow" src="/images/arrow.svg" alt=""> -->
        </div>
      </a>
      {{-- <p class="header__mode">{{ __('content.Круглосуточная поддержка') }}</p> --}}
    </div>
    <div class="header__social">
        <a class="header__socialLink" href="{{ Settings::get('apple') }}" target="_blank">
          <img class="header__socialImg" src="{{ asset('images/apple.png') }}" alt="">
        </a>
        <a class="header__socialLink" href="{{ Settings::get('play_market') }}" target="_blank">
          <img class="header__socialImg" src="{{ asset('images/google-play.png') }}" alt="">
        </a>
        <a class="header__socialLink" href="{{ Settings::get('facebook') }}" target="_blank">
          <img class="header__socialImg" src="{{ asset('images/face.png') }}" alt="">
        </a>
        <a class="header__socialLink" href="{{ Settings::get('instagram') }}" target="_blank">
          <img class="header__socialImg" src="{{ asset('images/insta.png') }}" alt="">
        </a>
    </div>
    <div class="header__lang">
      <!-- <ul class="header__lang--list">
        <li>
          <a href="">RU</a>
        </li>
        <li>
          <a href="">KZ</a>
        </li>
        <li>
          <a href="">ENG</a>
        </li>
      </ul> -->
      <ul class="main-nav__list">
        <li class="main-nav__item">
          <a class="main-nav__link">{{ mb_strtoupper(LaravelLocalization::getCurrentLocale()) }}
          </a>
          <div class="main-nav__drop">
            <ul>
              <li>
                  @foreach (LaravelLocalization::getSupportedLanguagesKeys() as $local)
                  @if ($local != LaravelLocalization::getCurrentLocale())
                      <a href="{{ LaravelLocalization::getLocalizedURL($local, request()->path() ).(Request::getQueryString() ? ('?' . Request::getQueryString()) : '') }}">{{ mb_strtoupper($local) }}</a>
                  @endif
                  @endforeach
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-nav">
    <div class="main-nav__row">
      <div class="main-nav__logo">
        <a href="{{ route('home') }}">
          <img src="{{ asset('images/logo.svg') }}" alt="">
        </a>
      </div>
      <!-- <div class="main-nav__menu"> -->
      <ul class="main-nav__list">
        <li class="main-nav__item">
          <a class="main-nav__link" href="/about">{{ __('content.О медцентре') }}
            <span>
              <img class="main-nav__arrow" src="{{ asset('images/down.png') }}" alt="">
            </span>
          </a>
          <div class="main-nav__drop">
            <ul>
              <li>
                <a href="{{ route('history') }}">{{ __('content.История Компании') }}</a>
              </li>
              <!--<li>
                <a href="#">История Компании</a>
              </li>
              <li>
                <a href="{{ route('news') }}">Новости</a>
              </li>-->
              <li>
                <a href="/carier">{{ __('content.Карьера') }}</a>
              </li>
				<li>
                <a href="{{ route('gallery') }}">{{ __('content.Галерея') }}</a>
              </li>
            </ul>
          </div>
        </li>

        <li class="main-nav__item">
          <a class="main-nav__link" href="{{ route('departments') }}">{{ __('content.Услуги') }}
            <span>
              <img class="main-nav__arrow" src="{{ asset('images/down.png') }}" alt="">
            </span>
          </a>
          <div class="main-nav__drop">
              <div class="row">
                  @foreach($menuDepartments->chunk(10) as $items)
                  <div class="col-sm-4">
                    <ul style="padding-bottom: 0px;">
                      @foreach($items as $item)
                      <li>

                        <a href="{{ $item['url'] }}">{{ $item['title'] }}</a>

                      </li>
                      @endforeach
                    </ul>
                </div>
                @endforeach
            </div>
          </div>
        </li>
		<li class="main-nav__item">
          <a class="main-nav__link" href="{{ route('price') }}">{{ __('content.Прейскурант') }}</a>
        </li>
        <li class="main-nav__item">
          <a class="main-nav__link" href="{{ route('doctors')}}">{{ __('content.Наша Команда') }}</a>
        </li>
        <li class="main-nav__item">
          <a class="main-nav__link" href="{{ route('news') }}">{{ __('content.Новости') }}</a>
        </li>

        <li class="main-nav__item">
          <a class="main-nav__link" href="{{ LaravelLocalization::getCurrentLocale()!='ru'?('/'.LaravelLocalization::getCurrentLocale()):'' }}/contact">{{ __('content.Контакты') }}</a>
        </li>
      </ul>
      <!-- </div> -->
      <div class="main-nav__btnWrap">
        <button id = "modalBtn" class="main-nav__btn" style="border: none; cursor: pointer;">
          {{ __('content.Записаться на прием') }}
        </button>
      </div>
    </div>
  </div>
</header>


<article class="mob-header">
  <input id="mob-header__checkbox" type="checkbox" class="mob-header__checkbox">
  <label for="mob-header__checkbox" class="mob-header__label">
    <span class="mob-header__icon">&nbsp;</span>
  </label>
  <div class="mob-header__background">&nbsp;</div>
  <!-- <div class="mob-header__bgImgWrp">
        <img src="http://arlangrip.kz/images/gallery-bg.png" alt="" class="mob-header__bgImg">
    </div> -->
  <nav class="mob-header__nav">
    <ul class="mob-header__list">
      <li class="mob-header__item mob-header__item--under">
        <a class="mob-header__link">{{ __('content.О медцентре') }}
          <span>
            <img class="main-nav__arrow" src="/images/arrow.svg" alt="">
          </span>
        </a>
        <div class="mob-header__drop">
          <ul>
            <!-- <li>
              <a href="/about">О нас</a>
            </li> -->
            <li>
              <a href="{{ route('history') }}">{{ __('content.История Компании') }}</a>
            </li>
            <li>
              <a href="{{ route('news')}}">{{ __('content.Новости') }}</a>
            </li>
            <li>
                <a href="/carier">{{ __('content.Карьера') }}</a>
              </li>
          </ul>
        </div>
      </li>
      <li class="mob-header__item mob-header__item--under">
        <a class="mob-header__link" href="{{ route('departments') }}">{{ __('content.Услуги') }}</a>
      </li>
	  <li class="mob-header__item">
        <a class="mob-header__link" href="{{ route('price') }}">{{ __('content.Прейскурант') }}</a>
      </li>
      <li class="mob-header__item">
        <a class="mob-header__link" href="{{ route('doctors')}}">{{ __('content.Наша Команда') }}</a>
      </li>
      <li class="mob-header__item">
        <a class="mob-header__link" href="{{ route('news') }}">{{ __('content.Новости') }}</a>
      </li>

      <li class="mob-header__item">
        <a class="mob-header__link" href="{{ LaravelLocalization::getCurrentLocale()!='ru'?('/'.LaravelLocalization::getCurrentLocale()):'' }}/contact">{{ __('content.Контакты') }}</a>
      </li>
      <li class="mob-header__item mob-header__item--under">
        <a class="mob-header__link"> RU
          <span>
            <img class="main-nav__arrow" src="/images/arrow.svg" alt="">
          </span>
        </a>
        <div class="mob-header__drop">
          <ul>
            <li>
              <a href="">KZ</a>
            </li>
            <li>
              <a href="">ENG</a>
            </li>
          </ul>
        </div>
      </li>
      <li class="mob-header__item mob-header__item--social">
          <a class="header__socialLink" href="{{ Settings::get('apple') }}">
            <img class="header__socialImg" src="{{ asset('images/apple.png') }}" alt="">
          </a>
          <a class="header__socialLink" href="{{ Settings::get('play_market') }}">
            <img class="header__socialImg" src="{{ asset('images/google-play.png') }}" alt="">
          </a>
          <a class="header__socialLink" href="{{ Settings::get('facebook') }}">
            <img class="header__socialImg" src="{{ asset('images/face.png') }}" alt="">
          </a>
          <a class="header__socialLink" href="{{ Settings::get('instagram') }}">
            <img class="header__socialImg" src="{{ asset('images/insta.png') }}" alt="">
          </a>
	  </li>
    </ul>
  </nav>
</article>
