@if ($paginator->hasPages())

    @if ($paginator->onFirstPage())
	<a href="#">
		<img src="/images/Group%2030.png">
	</a>
    @else
        
	<a href="{{ $paginator->previousPageUrl() }}">
		<img src="/images/Group%2030.png">
	</a>
    @endif    

    @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
        @if (is_string($element))
          
		<span class="active">{{ $element }}</span>
        @endif

        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    
                    <a class="active" href="#">{{ $page }}</a>
                @else
                    <a href="{{ $url }}">{{ $page }}</a>
	
                    
                @endif
            @endforeach
        @endif
    @endforeach
    @if ($paginator->hasMorePages())
            
			<a href="{{ $paginator->nextPageUrl() }}" rel="next">
                <img src="/images/Group%2032.png">
            </a>
    @else
        <a href="#" rel="next">
                <img src="/images/Group%2032.png">
            </a>
    @endif

    

@endif

