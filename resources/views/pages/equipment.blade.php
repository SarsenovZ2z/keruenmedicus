@extends('master') 

@section('content')
<main>
    <div class="equip"></div>
    <div class="container equipment">
        <div class="row">
            <div class="col-sm-4">
            <h1>{{ $page->block_1_title_1 }}</h1>
            </div>
            <div class="col-sm-8">
                <p>{!! $page->block_1_description_1 !!}</p>

            </div>
        </div>
    </div>
    <div class="container eq-types">
        <ul>
            <li class="equipment-list">
                <span>Диагностическое оборудование</span>

                <div class="hidden-eq">
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Аппарат УЗИ SIUI Apogee 5500</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Аппарат УЗИ SIUI Apogee 5500</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>

                </div>
            </li>

            <li class="equipment-list">
                <span>Хирургическое оборудование</span>
                <div class="hidden-eq">
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Оборудование</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Оборудование</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>

                </div>
            </li>

            <li class="equipment-list">
                <span>Терапевтическое оборудование</span>
                <div class="hidden-eq">
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Аппарат УЗИ SIUI Apogee 5500</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Аппарат УЗИ SIUI Apogee 5500</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>

                </div>
            </li>

            <li class="equipment-list">
                <span>Стационарное оборудование</span>
                <div class="hidden-eq">
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Оборудование</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>Оборудование</h4>
                            <p>Ультразвуковой сканер Apogee 5500 воспроизводит изображения премиум класса с исключительным разрешением
                                и улучшенным проникновением. Области применения: абдоминальные исследования, акушерство,
                                гинекология, поверхностные органы, сосуды, кардиология, педиатрия, урология, кардиология
                                педиатрическая, ортопедия, сосуды головного мозга.</p>
                        </div>
                        <div class="col-sm-4">
                            <div class="apparatus-img">
                                <img src="images/fe9ba2f5efd05dcac7cccd4afba9570d.png" alt="">
                            </div>

                        </div>
                    </div>

                </div>
            </li>

        </ul>
    </div>
    </div>
    <div class="row pack-zvon">
        <div class="zvonok2">
            <div class="row ">
                <div class="col-sm-6">
                    <h3 class="zakaz-headers2">По всем вопросам звоните:</h3>
                    <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                </div>
                <div class="col-sm-6">
                    <form id="zakaz-form" action="" method="post">
                        <h3 class="zakaz-headers2">Или закажите обратный звонок:</h3>
                        <input type="text" class="input-zvon2" placeholder="Введите свой телефон" name="">
                        <input type="submit" id="zakazat2" name="zvonok" value="Заказать">
                    </form>
                </div>
            </div>
        </div>
    </div>

</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">

@endsection