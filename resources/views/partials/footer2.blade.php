<footer id="bs-footer" class="bs-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="bs-footer__logo">
          <a href="/">
            <img src="{{ asset('images/footer-logo.png') }}" alt="">
          </a>
        </div>
        <!-- <p class="bs-footer__text">Современный специализированный медицинский центр в Алматы
          <br> с широким спектром медицинских услуг.</p> -->
        <p class="bs-footer__text">{{ Settings::get('about_text') }}</p>
      </div>
      <div class="col-sm-3 col">
        <h5 class="bs-footer__head">{{ __("content.О Компании") }}</h5>
        <ul class="bs-footer__list">
          <li>
            <a href="/about" class="bs-footer__link">{{ __("content.О Нас") }}</a>
          </li>
          <li>
            <a href="{{ route('doctors') }}" class="bs-footer__link">{{ __("content.Наша Команда") }}</a>
          </li>
          <li>
            <a href="{{ route('gallery') }}" class="bs-footer__link">{{ __("content.Фото/видео галерея") }}</a>
          </li>
        </ul>
      </div>
      <div class="col-sm-3 col">
        <ul class="bs-footer__list bs-footer__list--sec">
          <li>
            <a href="{{ route('news') }}" class="bs-footer__link">{{ __("content.Новости") }}</a>
          </li>
          <li>
            <a href="{{ route('price') }}" class="bs-footer__link">{{ __("content.Цены") }}</a>
          </li>
          <li>
            <a href="/contacts" class="bs-footer__link">{{ __("content.Контакты") }}</a>
          </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="bs-footer__head">{{ __("content.Адрес") }}</h5>
        <p class="bs-footer__text">{{ Settings::get('address') }}</p>
        <h5 class="bs-footer__head">{{ __("content.Телефон") }}:</h5>
        <a href="tel:{!! Settings::get('phones') !!}" class="bs-footer__link">{!! Settings::get('phones') !!}</a>
        <h5 class="bs-footer__head">{{ __("content.Email") }}:</h5>
        <a href="mailto:info@keruen-medicus.kz" class="bs-footer__link">{{ Settings::get('email') }}</a>
      </div>
    </div>
    <div class="row bs-footer__row">
      <div class="col-sm-9">
        <p class="bs-footer__text">{{ Settings::get('copyright') }}</p>
      </div>
      <div class="col-sm-3">
        <div class="bs-footer__social">
          <a class="header__socialLink" href="{{ Settings::get('apple') }}" target="_blank">
            <img class="header__socialImg" src="{{ asset('images/apple.png') }}" alt="">
          </a>
          <a class="header__socialLink" href="{{ Settings::get('play_market') }}" target="_blank">
            <img class="header__socialImg" src="{{ asset('images/google-play.png') }}" alt="">
          </a>
          <a class="header__socialLink" href="{{ Settings::get('facebook') }}" target="_blank">
            <img class="header__socialImg" src="{{ asset('images/face.png') }}" alt="">
          </a>
          <a class="header__socialLink" href="{{ Settings::get('instagram') }}" target="_blank">
            <img class="header__socialImg" src="{{ asset('images/insta.png') }}" alt="">
          </a>
        </div>
      </div>
    </div>
  </div>
</footer>
