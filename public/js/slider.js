

$('.single-item').slick({
	dots: true,
	arrows: false,
	autoplay:true,
  autoplaySpeed:10000,
  pause: 3000,
 
});
$('.one-time').slick({
  dots: true,
  arrows: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true,
  asNavFor: '.slider-cat'
});
if ($(window).width() < 992) {
$('.slider-cat').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.one-time',
  dots: false,
  arrows: false
});
}
  $('.one-times').slick({
  dots: true,
  arrows: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true
});

$('.multiple-items').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true,
  responsive: [
    {
      breakpoint: 1280,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

// jQuery('.single-item').on('beforeChange', function(event, slick, currentSlide, nextSlide){
//     if(window.outerWidth > 700){
//         var background = "" + $('.slider-info').data('bck');
//         $('.jumbotron').css("background-image",background);
//     }
// });
