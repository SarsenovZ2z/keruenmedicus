<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}'><i class='fa fa-cog'></i> <span>Настройки</span></a></li>
<li><a href="{{backpack_url('page') }}"><i class="fa fa-file-o"></i> <span>Страницы</span></a></li>
<li><a href="{{backpack_url('history') }}"><i class="fa fa-file-o"></i> <span>История (О нас)</span></a></li>


<li><a href="{{backpack_url('doctor') }}"><i class="fa fa-file-o"></i> <span>Врачи</span></a></li>
<li><a href="{{backpack_url('specialization') }}"><i class="fa fa-file-o"></i> <span>Специализации</span></a></li>
<li><a href="{{backpack_url('department') }}"><i class="fa fa-file-o"></i> <span>Отделения</span></a></li>
<li><a href="{{backpack_url('gallery') }}"><i class="fa fa-file-o"></i> <span>Галерея</span></a></li>
<li><a href="{{backpack_url('post') }}"><i class="fa fa-file-o"></i> <span>Новости</span></a></li>
<li><a href="{{backpack_url('vacancy') }}"><i class="fa fa-file-o"></i> <span>Вакансии</span></a></li>
<li><a href="{{backpack_url('sale') }}"><i class="fa fa-file-o"></i> <span>Акции</span></a></li>
<li><a href="{{backpack_url('check') }}"><i class="fa fa-file-o"></i> <span>Чек-ап</span></a></li>
<li><a href="{{backpack_url('price') }}"><i class="fa fa-file-o"></i> <span>Прайс</span></a></li>
<li><a href="{{backpack_url('package') }}"><i class="fa fa-file-o"></i> <span>Пакеты</span></a></li>
{{-- <li><a href="{{backpack_url('review') }}"><i class="fa fa-file-o"></i> <span>Отзывы</span></a></li> --}}
<li><a href="{{backpack_url('slider') }}"><i class="fa fa-file-o"></i> <span>Слайдеры</span></a></li>
