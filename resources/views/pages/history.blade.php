@extends('master-new')


@section('content')

    <!-- You write code for this content block in another file -->

{{--<div class="bs-history">
  <div>
    <h2 class="bs-history__head">История Компании</h2>
  </div>

  <div class="bs-history__mid">
    <div class="bs-history__mid-line">

    </div>
    <!--  <div class="bs-history__mid-dots">
      <div class="bs-history__dot"></div>
      <div class="bs-history__dot"></div>
      <div class="bs-history__dot bs-history__dot--active"></div>
      <div class="bs-history__dot"></div>
    </div> -->
    <div class="infra__slider infra__mid-dots-names">
      @foreach($histories as $key=>$history)
      @php
        $key++;
      @endphp
      <div>
        <div class="infra__dot-name" data-index="{{$key}}" data-id="y{{ $history->title }}">{{ $history->title }}000</div>
      </div>

      @endforeach
    </div>
  </div>
   <div class="infra__img-wrp">
    <p class="infra__txt">Выпуск первого номера бесплатной газеты “Медицинские Технологии Казахстана”. Издание стало выходить ежемесячно</p>
  </div>
  <div class="bs-history__div">
    <div class="infra__mid-slider">
        @foreach($histories as $history)
      <div class="bs-history__info" id="y{{$history->title}}">
        <p>{{$history->content}}0000</p>
      </div>
      @endforeach

    </div>
  </div>
</div>

--}}
<!------------------------------ NEW -------------------------->

<div class="bs-history">
  <div class="container">
    <div>
      <h2 class="bs-history__head">История Компании</h2>
    </div>

    <div class="bs-history__mid">
      <div class="bs-history__mid-line"></div>
      <div class="infra__slider infra__mid-dots-names">
        @foreach ($histories as $history)
            <div class="row ">
              <div class="infra__dot-name {{ ($loop->iteration%2)==0?'infra__dot-name--right':'' }} infra__dot-name--active" data-index="{{ $loop->iteration }}" data-id="y2004">{{ $history->title }}</div>
              <div class="bs-history__dot bs-history__dot--active"></div>
              <div class="bs-history__info {{ $loop->iteration%2==1?'right':'left' }}" id="y2004">
    			  <img class="bs-history__info-img" src="/uploads/{{ $history->image }}">
                <p> {!! $history->content !!} </p>
              </div>
            </div>
        @endforeach
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>

@endsection
