@extends('master') 

@section('content')
<main>
  <div class="container all-docs">
    <div class="row">
      <div class="col-sm-3">
        <h3 class="category-head">Врачи</h3>
        <ul class="category-list">
          {{-- <li class="category-item category-item--active">
            <a data-ref="surgery" href="">Хирурги</a>
          </li> --}}
          @foreach($specializations as $item)
          <li class="category-item">
          <a data-ref="{{ $item->slug }}" href="" class="doctor-filter">{{ $item->title }}</a>
          </li>
          @endforeach
        </ul>
      </div>
      <div class="col-sm-9">
        <div id="all" class="all-doctors all-doctors--active" style="display:none;">
          @foreach($specializations as $specialization)
          @foreach($specialization->doctors->chunk(4) as $doctors)
          <div class="row">
            @foreach($doctors as $doctor)
            <div class="col-sm-3">
                <img src="{{ asset('uploads/' . $doctor->image) }}" alt="{{ $doctor->title }}">
                  <h4 class="doc-name">{{ $doctor->title }}</h4>
                  <a class="doc-btn" href="{{ route('doctor', ['docSlug' => $doctor->slug]) }}">Подробнее</a>
                </div>
            @endforeach
          </div>
          @endforeach
          @endforeach
        </div>
    <div id="all-without-filter" class="all-doctors all-doctors--active">
          @foreach($doctorsSymbat->chunk(4) as $chunkDoctors)
          <div class="row">
            @foreach($chunkDoctors as $doctor)
            <div class="col-sm-3">
                <img src="{{ asset('uploads/' . $doctor->image) }}" alt="{{ $doctor->title }}">
                  <h4 class="doc-name">{{ $doctor->title }}</h4>
                  <a class="doc-btn" href="{{ route('doctor', ['docSlug' => $doctor->slug]) }}">Подробнее</a>
                </div>
            @endforeach
          </div>
          @endforeach
        </div>
        @foreach($specializations as $item)
        <div id="{{ $item->slug }}" class="all-doctors">
          @foreach($item->doctors->chunk(4) as $doctors)
          <div class="row">
            @foreach($doctors as $doctor)
            <div class="col-sm-3">
            <img src="{{ asset('uploads/' . $doctor->image) }}" alt="{{ $doctor->title }}">
              <h4 class="doc-name">{{ $doctor->title }}</h4>
              <a class="doc-btn" href="{{ route('doctor', ['docSlug' => $doctor->slug]) }}">Подробнее</a>
            </div>
            @endforeach
          </div>
          @endforeach
        </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="mobAll-docs">

    <ul class="cat-list">
      <h3 class="category-head">Врачи</h3>
      @foreach($specializations as $specialization)
      <li class="cat-item">
        <p class="category-name">{{ $specialization->title }}</p>
        <span>
        <img src="{{ asset('images/downs.png') }}" alt="{{ $specialization->title }}">
        </span>
        <div class="docs-mob">
          @foreach($specialization->doctors->chunk(2) as $doctors)
          <div class="row">
            @foreach($doctors as $doctor)
            <div class="col-sm-6">
            <img src="{{ asset('uploads/'.$doctor->image) }}" alt="{{ $doctor->title }}">
              <h4 class="doc-name">{{ $doctor->title }}</h4>
              <a class="doc-btn" href="{{ route('doctor', ['docSlug' => $doctor->slug]) }}">Подробнее</a>
            </div>
            @endforeach
          </div>
          @endforeach
        </div>
      </li>
      @endforeach

    </ul>
  </div>
</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">

@endsection

@section('after_jquery')
<script>
 $('.doctor-filter').click(function() {

  $('#all-without-filter').hide();
 });
</script>

@endsection
