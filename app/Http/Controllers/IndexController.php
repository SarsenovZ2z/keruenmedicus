<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SEO;
use OpenGraph;
use App\Models\Page;
use App\Models\Doctor;
use App\Models\Specialization;
use App\Models\Department;
use App\Models\Gallery;
use App\Models\Slider;
use App\Models\Check;
use App\Models\Package;
use App\Models\Post;
use App\Models\Price;
use App\Models\History;
use App\Models\Vacancy;
use Share;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class IndexController extends Controller
{
    public function index($slug)
    {
        $pages = Page::findBySlug($slug);

        if (!$pages)
        {
            abort(404, 'Please go bawdack to our <a href="'.url('').'">homepage</a>.');
        }

        $title = $pages->title;
        $page = $pages->withFakes();
        SEO::setTitle($page->meta_title);
        SEO::setDescription($page->meta_description);
        SEO::opengraph()->setUrl(asset(''.$page->slug));
        SEO::setCanonical(asset(''.$page->slug));
        SEO::opengraph()->addProperty('type', 'site');

        $histories = History::orderBy('lft')->where('active', 1)->get();

        return view('pages.'.$page->template, compact('page', 'title', 'histories'));
    }

    public function local()
    {
        // $items = \App\Models\Setting::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('value', 'ru', $item->value);
        //     $item->save();
        // }
        // echo "Settings done;<br/>";
        //
        // $items = Check::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->setTranslation('description', 'ru', $item->description);
        //     $item->save();
        // }
        // echo "Checks done;<br/>";
        //
        // $items = Department::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->save();
        // }
        // echo "Departments done;<br/>";
        //
        // $items = Doctor::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('experience', 'ru', $item->experience);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->save();
        // }
        // echo "Doctors done;<br/>";
        //
        // $items = Gallery::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->save();
        // }
        // echo "Gallery done;<br/>";
        //
        // $items = History::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->save();
        // }
        // echo "History done;<br/>";

        // $items = Package::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->save();
        // }
        // echo "Packages done;<br/>";
        //
        // $items = Post::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->save();
        // }
        // echo "Posts done;<br/>";
        //
        // $items = Price::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('price', 'ru', $item->price);
        //     $item->save();
        // }
        //
        // $items = \App\Models\Sale::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->save();
        // }
        // echo "Sales done;<br/>";
        //
        // $items = Slider::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->setTranslation('content', 'ru', $item->content);
        //     $item->save();
        // }
        // echo "Sliders done;<br/>";
        //
        // $items = Specialization::all();
        // foreach ($items as $item)
        // {
        //     $item->setTranslation('title', 'ru', $item->title);
        //     $item->save();
        // }
        // echo "Specializations done;<br/>";

    }

	public function history()
    {
        $histories = History::orderBy('lft')->where('active', 1)->get();

        return view('pages.history', compact('histories'));
    }

    public function home()
    {
        $page = Page::where('template', 'home')->first();

        $checks = Check::orderBy('lft')->where('active', 1)->get();

        $sliders = Slider::latest()->where('active', 1)->get();
        $departments = Department::orderBy('lft')->where('active', 1)->whereNull('parent_id')->get();
        $packages = Package::orderBy('lft')->where('active', 1)->whereNull('parent_id')->get();

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();

        $page =  $this->data['page'];
        SEO::setTitle($page->meta_title);
        SEO::setDescription($page->meta_description);
        SEO::opengraph()->setUrl(asset(''.$page->slug));
        SEO::setCanonical(asset(''.$page->slug));
        SEO::opengraph()->addProperty('type', 'site');

        if (!$page)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }



        return view('pages.home', compact('pageTitle', 'page', 'checks', 'sliders', 'departments', 'packages'));
    }

    public function homeNew()
    {
        $page = Page::where('template', 'home')->first();

        $checks = Check::orderBy('lft')->where('active', 1)->get();

        $sliders = Slider::latest()->where('active', 1)->get();
        $departments = Department::orderBy('lft')->where('active', 1)->whereNull('parent_id')->take(15)->get();
        $packages = Package::orderBy('lft')->where('active', 1)->whereNull('parent_id')->get();

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();

        $page =  $this->data['page'];
        SEO::setTitle($page->meta_title);
        SEO::setDescription($page->meta_description);
        SEO::opengraph()->setUrl(asset(''.$page->slug));
        SEO::setCanonical(asset(''.$page->slug));
        SEO::opengraph()->addProperty('type', 'site');

        if (!$page)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        return view('pages.home-new', compact('pageTitle', 'page', 'checks', 'sliders', 'departments', 'packages'));
    }

    public function doctors()
        {
            $doctorsSymbat = Doctor::orderBy('lft')->get();
            $specializations = Specialization::orderBy('lft')->with(['doctors' => function($query) {
                $query->orderBy('lft')->get();
            }])->get();

            return view('pages.doctors', compact('specializations', 'doctorsSymbat'));
        }
	public function doctor($docSlug)
    {
        $doctor = Doctor::where('slug', $docSlug)->where('active', 1)->first();

        return view('pages.doctor', compact('doctor'));
    }

    public function departments()
    {
        $department = Department::orderBy('lft')->first();

        return redirect()->route('department', ['depSlug' => $department->slug]);
    }

    public function department($depSlug)
    {
        $department = Department::where('active', 1)->where('slug', $depSlug)->first();

		$next = Department::where('id', '>', $department->id)->orderBy('id')->select('slug')->first();
		$previous = Department::where('id', '<', $department->id)->orderBy('id', 'desc')->select('slug')->first();
        $departments = Department::orderBy('lft')->where('active', 1)->get();
        $menuInDepartments = Department::orderBy('lft')->where('active', 1)->whereNull('parent_id')->get();
        SEO::setTitle($department->title);
        OpenGraph::addImage(asset('uploads/' . $department->image));
        OpenGraph::setTitle($department->title);
        $share = Share::load(url()->current(), $department->title, isset($department->image) ? asset('uploads/' . $department->image): '')->services('facebook', 'vk', 'twitter');
        return view('pages.department', compact('department', 'departments', 'menuInDepartments', 'share', 'next', 'previous'));
    }

    public function gallery()
    {
        //$firstThree = Gallery::orderBy('lft')->where('active', 1)->limit(3)->get();
        $photos = Gallery::orderBy('lft')->where('active', 1)->where('select', 'image')->get();
        $videos = Gallery::orderBy('lft')->where('active', 1)->where('select', 'video')->get();
       // dd($other);

        return view('pages.gallery', compact(['photos', 'videos']));
    }

    public function news()
    {
        $news = Post::latest()->where('active', 1)->paginate(12);

        return view('pages.news.news', compact('news'));
    }

    public function post($postSlug)
    {
        $post = Post::where('active', 1)->where('slug', $postSlug)->first();

        if (!$post)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

		$news = Post::latest()->where('active', 1)->where('id', '!=', $post->id)->take(10)->get();

        SEO::setTitle($post->title);
        // SEO::setDescription($page->meta_description);


        return view('pages.news.index', compact('post', 'news'));
    }

    public function price(Request $request)
    {


            $prices = Price::latest()->where('active', 1)->get();
            return view('pages.price', compact('prices'));


    }

    public function checks()
    {
        $checks = Check::orderBy('lft')->where('active', 1)->get();
		$menuInDepartments = Department::orderBy('lft')->where('active', 1)->whereNull('parent_id')->get();

        return view('pages.checkup', compact('checks', 'menuInDepartments'));
    }

    public function packages()
    {
        $packages = Package::orderBy('lft')->where('active', 1)->whereNull('parent_id')->get();

        return view('pages.packages', compact('packages'));
    }

    public function package($packSlug)
    {
        $package = Package::where('active', 1)->where('slug', $packSlug)->first();
        $parentPackages = Package::where('active', 1)->whereNull('parent_id')->get();
        $allPackages = Package::orderBy('lft')->where('active', 1)->whereNotNull('parent_id')->get();
        SEO::setTitle($package->title);
        OpenGraph::addImage(asset('uploads/' . $package->image));
        OpenGraph::setTitle($package->title);
        $share = Share::load(url()->current(), $package->title, isset($package->image) ? asset('uploads/' . $package->image): '')->services('facebook', 'vk', 'twitter');
        return view('pages.package', compact('package', 'parentPackages', 'allPackages', 'share'));
    }

    public function phoneForm(Request $request)
    {
        Mail::send('email.phone', $request->all(), function ($message)  {
            $message->from('keruen.mailer@gmail.com', 'Keruen Medicus');
            $message->to(Setting::get('feedback_email'), 'Keruen')->subject('Заявка на обратный звонок | Keruen Medicus');
            // $message->cc('balymbetov.temirlan@gmail.com', '');
        });

        return redirect()->back()->with('flash_message_phone', 'Ваша заявка на обратный звонок успешно отправлена!');
    }

    public function simpleForm(Request $request)
    {
        Mail::send('email.simple', $request->all(), function ($message)  {
            $message->from('keruen.mailer@gmail.com', 'Keruen Medicus');
            $message->to(Setting::get('feedback_email'), 'Keruen')->subject('Заявка на прием | Keruen Medicus');
            // $message->cc('balymbetov.temirlan@gmail.com', '');
        });

        return redirect()->back()->with('flash_message_simple', 'Ваша заявка на прием успешно отправлена!');
    }

	public function search(Request $request)
    {
        $q = $request->get('q');
        if(isset($q)) {
                // Новости
            $news = Post::search($q)->where('active', 1)->get();

            // Департаменты
            $departments = Department::search($q)->where('active', 1)->get();

            //Врачи
            $doctors = Doctor::search($q)->where('active', 1)->get();

            // Чек-апы
            $checks = Check::search($q)->where('active', 1)->get();

            //Пакеты
            $packages = Package::search($q)->where('active', 1)->get();

			//Prices
            $prices = Price::search($q)->where('active', 1)->get();

            return view('pages.search', compact(['news','departments','doctors','checks','q', 'packages', 'prices']));


            return view('pages.search', compact(['news','departments','doctors','checks','q', 'packages']));
        } else {
            return redirect()->back();
        }

    }

	public function fixedForm(Request $request)
	{

		$file = $request->all()['file'];
		$fileName = '';
		if(isset($file)) {
			$extension = $file->getClientOriginalExtension();
			Storage::disk('uploads')->put('form/' . $file->getFilename().'.'.$extension,  File::get($file));
			$fileName = asset('uploads/' . 'form/' . $file->getFilename().'.'.$extension);
		}
		$data = [
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'content' => $request->get('content'),
			'fileName' => $fileName
		];

		Mail::send('email.fixed', $data, function ($message)  {
            $message->from('keruen.mailer@gmail.com', 'Keruen Medicus');
            $message->to(Setting::get('feedback_email'), 'Keruen')->subject('Связаться с нами | Keruen Medicus');
            // $message->cc('balymbetov.temirlan@gmail.com', '');
        });

		return redirect()->back()->with('flash_message_fixed', 'Ваша заявка на прием успешно отправлена!');
    }


    public function carier(Request $request)
    {
        $data['vacancies'] = Vacancy::orderBy('lft')->get();
        $data['page'] = Page::findBySlug('carier');
        $page = $data['page'];
        $data['data'] = json_decode($page->extras);
        return view('pages.carier', $data);
    }

    public function searchPrice(Request $request)
    {
        if($request->ajax())
        {

            $first="";
            $second="";
            $third="";
            $empty = '';
            $prices = Price::search($request->search)->latest()->where('active', 1)->get();

            if($prices)
            {

                foreach ($prices as $key => $price) {
                    $first.='<li class="price__item" data-index="'.$key .'">'.$price->title .'</li>';
                    $second.= '<div class="price__tab" data-index="'. $key .'"><div class="price__row">
                      <div class="price__col">
                        <span class="price__heading price__heading--name">Терапевтический профиль</span>
                      </div>
                      <div class="price__col">
                        <span class="price__heading">Процедура</span>
                      </div>
                      <div class="price__col">
                        <span class="price__heading">Цена</span>
                      </div>
                    </div>';
                    if(isset($price->price)) {
                        foreach(json_decode($price->price) as $item) {
                            if(isset($item->name)) {
                                $priceName = $item->name;
                            } else {
                                $priceName = '';
                            }

                            if(isset($item->procedure)) {
                                $priceProcedure = $item->procedure;
                            } else {
                                $priceProcedure = '';
                            }

                            if(isset($item->price)) {
                                $pricePrice = $item->price;
                            } else {
                                $pricePrice = '';
                            }

                            $second.='<div class="price__row">
                            <div class="price__col">
                                <span class="price__service">'.$priceName .'</span>
                            </div>
                            <div class="price__col">
                                <span class="price__procedure">'. $priceProcedure .'</span>
                            </div>
                            <div class="price__col">
                                <span class="price__cost">'. $pricePrice .'</span>
                            </div>
                            </div>';
                        }
                    }
                    $second.='</div>';

                    $third.=' <div class="price__tab" data-index="'. $key .'">
                    <div class="price__header">
                        <button class="price__bck-icon">
                        <img src="/images/prev.svg" alt="back">
                        </button>
                        <h5 class="price__head">'. $price->title .'</h5>
                    </div>';
                    if(isset($price->price)) {
                        foreach(json_decode($price->price) as $item) {
                            if(isset($item->name)) {
                                $priceName = $item->name;
                            } else {
                                $priceName = '';
                            }

                            if(isset($item->procedure)) {
                                $priceProcedure = $item->procedure;
                            } else {
                                $priceProcedure = '';
                            }

                            if(isset($item->price)) {
                                $pricePrice = $item->price;
                            } else {
                                $pricePrice = '';
                            }
                        $third.= '<div class="price__row-mobile">
                        <p class="price__name">'. $priceName .'</p>
                        <div>
                        <span class="price__procedure-mobile">'. $priceProcedure .'</span>
                        <span class="price__cost-mobile">'. $pricePrice .'</span>
                        </div>
                        </div>';
                        }
                    }
                    $third.='</div>';

                }

                $output = [
                    'first' => $first,
                    'second' => $second,
                    'third' => $third
                ];
            return Response($output);
            }
        }
    }
}
