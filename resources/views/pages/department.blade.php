@extends('master')

@section('content')
<main role="main">
    <div class="slider-cat">
        <img class="big-img" src="{{ asset('uploads/' . $department->image) }}">
        <div class="cat-overlay"></div>
		@foreach($menuDepartments as $dep)
		<div>
			@if(isset($dep['image']))
        	<img class="mob-img" src="{{$dep['image']}}">
			@else
			<img class="mob-img" src="/uploads/departments/c7f9046291db26a2afffa1c703fd3446.png">
			@endif
		</div>
		@endforeach


    <h1 class="slider-header">{{ $department->title }}</h1>
    </div>
    <div class="department">
        <div class="container">
			<div class="one-times">
            @foreach($menuDepartments->chunk(5) as $items)
            <div class="row">
                @foreach($items as $item)
                <a class="dep-link" href="{{ $item['url'] }}">
                    <div class="col-sm-2">
                        <div class="depWrap">
                        <img src="{{ $item['icon'] }}" alt="{{ $item['title'] }}">
                        </div>
                        <p class="dep-name">{{ $item['title'] }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            @endforeach
			</div>
        </div>
    </div>
	<div class="sliderMobile">
        <h2 class="kind">{{ __("content.Направление") }}</h2>
        <div class="one-time">
			@foreach($menuDepartments as $key=>$dep)
          	<div>
            	<a href="{{ $dep['url'] }}">
              		<div class="overlay">
                		<img id="img{{$key}}" src="{{ $dep['icon'] }}" alt="{{ $dep['title'] }}">
              		</div>
              		<p class="card-text1">{{ $dep['title'] }}</p>
            	</a>
          	</div>
			@endforeach
        </div>
      </div>
    <div class="container category-container">
        <div class="row category-row">
            <div class="col-sm-6 desc">
                <div class="container">

                    <div class="row text-img">
                        {!! $department->content !!}

                    </div>
                    <img class="del-line" width="750.84" src="/images/Line%209.png">

                    <div class="row social">
                        <div class="col-sm-6 heading">
                            <p>{{ __("content.Поделиться с друзьями") }}:</p>
                        </div>
                        <div class="col-sm-6 icons">
                            <div class="row">
                                <div class="col-sm-3">

                                        <a target="_blank" href="{{ $share['facebook'] }}">
                                        <img src="/images/facebook.png">
                                        </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="{{ $share['twitter'] }}">
                                        <img src="/images/twitter.png" >
                                    </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="{{ $share['vk'] }}">
                                        <img src="/images/vkontakte-logo.png" >
                                    </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl={{ urlencode(url()->current()) }}">
                                        <img src="/images/odnoklassniki-logo.png" >
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row ">
                        <div class="zvonok">
                            <h3 class="zakaz-headers">{{ __("content.По всем вопросам звоните") }}:</h3>
                            <p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                        <form id="zakaz-form" action="{{ route('form.phone') }}" method="post">
                                {{ csrf_field() }}
                                <h3 class="zakaz-headers">{{ __("content.Или закажите обратный звонок") }}:</h3>
                                <input type="text" class="input-zvon" placeholder="{{ __('content.Введите свой телефон') }}" name="phone" required>
                                <input type="submit" id="zakazat" name="zvonok" value="{{ __('content.Заказать') }}">
                            </form>
                        </div>
                    </div>






                    <img width="750.84" class="del-line" src="/images/Line%209.png">

                </div>
            </div>


            <div class="col-sm-6 sidebar">


                <div class="container">
                    <div class="row">
                        <div class="nav-side-menu">
                            <div class="menu-list">
                                <ul>
									<a href="{{ Settings::get('apple') }}" target="_blank" style="width: 100%;">
                                    	<img src="{{ asset('images/department-ban.png') }}" style="width: 100%;">
									</a>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row packet-type">
                        <a href="/packagess/rody">
                            <div class="row">
                                <img src="/images/Mask%20Group-2.png">
                                <h2 class="packet-name">Пакет "Роды"</h2>
                            </div>
                        </a>
                    </div> --}}


                    {{-- <div class="row spec-type">
                        <div class="spec">
                            <h2>Специалисты</h2>
                            <a href=""> Акушер-гинеколог</a>
                        </div>
                    </div> --}}

                </div>

            </div>
        </div>
    </div>



</main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
@endsection
