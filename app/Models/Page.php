<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;

class Page extends \Backpack\PageManager\app\Models\Page
{
    use HasTranslations;

    public $translatable = ['name', 'title', 'content', 'extras'];
}
