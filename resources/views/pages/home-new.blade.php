@extends('master-new')

@section('content')
<article class="main">
  <div class="slider-wrap">
    <div class="main__video-mask"></div>



    <ul class="constructionSlider">
        @foreach($sliders as $slider)
            <li>
                <div class="main__words">
                  <h3 class="main__head">{{ $slider->title }}</h3>
                  <p class="main__slideTxt">{{ $slider->content }}</p>
                </div>
                <img class="main__bans" src="{{ asset('uploads/' . $slider->image) }}" alt="{{ $slider->title }}">
                <div class="slider-mask"></div>
            </li>
        @endforeach
    </ul>

    <div class="bx-pager">
      <ul>
         @foreach($sliders as $key=>$slider)
        <li>
          <a data-slide-index="{{$key}}" href=""></a>
        </li>
        @endforeach
      </ul>
      <div class="progress">
        <span></span>
      </div>
    </div>
    <div class="slider-controls">
      <span id="slider-prev"></span>
      <span id="slider-next"></span>
    </div>
  </div>
  <div class="main__info">
    <div class="container main__digits">
      <div class="row">
        <div class="col-sm-3">
			<div class="main__number">
            <div class="main__counter-mask">
          <h2 class="main__digit bs-main__digit--counter counter1">{{ $page->block_1_title_1 }}</h2>
				</div>
			</div>
          <p class="main__digitInfo">{{ $page->block_1_heading_1 }}</p>
        </div>
        <div class="col-sm-3">
			<div class="main__number">
            <div class="main__counter-mask">
          <h2 class="main__digit bs-main__digit--counter counter2">{{ $page->block_1_title_2 }}</h2>
				</div>
			</div>
          <p class="main__digitInfo">{{ $page->block_1_heading_2 }}</p>
        </div>
        <div class="col-sm-3">
			<div class="main__number">
            <div class="main__counter-mask">
          <h2 class="main__digit bs-main__digit--counter counter3">{{ $page->block_1_title_3 }}</h2>
				</div>
			</div>
          <p class="main__digitInfo">{{ $page->block_1_heading_3 }}</p>
        </div>
        <div class="col-sm-3">
			<div class="main__number">
            <div class="main__counter-mask">
          <h2 class="main__digit bs-main__digit--counter counter4">{{ $page->block_1_title_4 }}</h2>
				</div>
			</div>
          <p class="main__digitInfo">{{ $page->block_1_heading_4 }}</p>
        </div>
      </div>
    </div>
    <div class="main__departments">
      <div class="container">
        <h3 class="main__chooseDep">Выберите направление</h3>
        <div class="row">
            @foreach($departments->chunk(5) as $hi=>$items)
              <div class="row {{ ($hi == '0') ? 'napravlenie' : '' }}">
                @foreach($items as $key=>$item)
                    <a href="{{ route('department', ['depSlug' => $item->slug]) }}">
                        <div class="col-sm-2">
                          <img class="main__depImg" src="{{ asset('uploads/' . $item->icon) }}" alt="{{ $item->title }}">
                        </div>
                        <p class="main__depName">{{ $item->title }}</p>
                    </a>
                @endforeach
              </div>
            @endforeach
        </div>
        <button class="main__depButton">
          <a href="{{ route('departments') }}">Все направления</a>
        </button>
      </div>
    </div>
  </div>
  <div class="main__programm">
    <div class="container">
      <h3 class="main__programm-head">{{ $page->block_2_title_1 }}</h3>
      <!-- <h3 class="main__programm-head">Программы
        <br> медицинских услуг</h3> -->
      <p class="main__programm-text">Клиника «Керуен-Medicus» предлагает медицинские программы для женщин и детей.</p>
      <p class="main__programm-text main__programm-text--big">Программы для женщин позволяют осуществлять медицинский контроль за состоянием женщины, тщательный контроль за состоянием
        плода, внутриутробную диагностику врожденных пороков развития
        <br> у плода и возможность экстренной медицинской помощи.</p>
      <!-- <p class="main__programm-text">{{ $page->block_2_description_1 }}</p> -->
      <div class="main__programm-blocks">
          @foreach($packages->chunk(3) as $items)
            <div class="row">
                @foreach($items as $key=>$item)
                  <div class="col-sm-4">
                    <a href="{{ route('package', ['packSlug' => $item->slug]) }}">
                      <div class="main__programm-col">
                        <figure>
                          <img  class="main__programm-img" id="image{{ $key }}" src="{{ isset($item->thumbnail) ? asset('uploads/' .  $item->thumbnail) :  asset('uploads/' .  $item->image)}}" alt="{{ $item->title }}">
                        </figure>
                        <h5 class="main__programm-title">{{ $item->title }}</h5>
                      </div>
                    </a>
                  </div>
                @endforeach
            </div>
          @endforeach
      </div>
    </div>
  </div>
  <div class="main__checkUp">
    <h3 class="main__checkUp-head">{{ $page->block_3_title_1 }}</h3>
    <p class="main__checkUp-p">{{ $page->block_3_description_1 }}</p>


    <div class="responsive">
        @foreach($checks as $item)
          <div class="main__checkUp-slide">
            <h5 class="main__checkUp-title">{{ $item->title }}</h5>
            <hr class="main__checkUp-line">
            <p class="main__checkUp-text">{!!  str_limit( strip_tags($item->description), 100) !!}</p>
            <div class="main__checkUp-link">
              <a href="{{ route('checks', ['ids' => $item->id]) }}">
                <img src="{{ asset('images/sliderArrow.svg') }}">
              </a>
            </div>
          </div>
         @endforeach
    </div>
  </div>

</article>

@endsection
