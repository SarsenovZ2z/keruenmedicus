<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    {!! SEO::generate() !!}
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="author" content="BrandStudio.kz">
    <link rel="icon" href="images/favicon.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="/css/main-new.css">
    <meta name="_token" content="{{ csrf_token() }}">
	<link href="{{ asset('css/album.css') }}" rel="stylesheet">
</head>

<body>
    <div id="orderModal" class="modal">

    <div class="modal-content" id="order-modal">
      <span class="close">&times;</span>
      <iframe src="https://company.medelement.com/%D0%BC%D0%B5%D0%B4%D0%B8%D1%86%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9-%D1%86%D0%B5%D0%BD%D1%82%D1%80-%D0%BA%D0%B5%D1%80%D1%83%D0%B5%D0%BD-medicus-%D0%B0%D0%BB%D0%BC%D0%B0%D1%82%D1%8B?keep_sections[]=online-entry" frameborder="0" border="0" width="100%" height="100%"></iframe>

    </div>

  </div>
  @if(Session::has('flash_message_fixed'))

   <div id="myModal2" class="modal" style="display:block">

  <div class="modal-content2">
    <span class="close2">&times;</span>
    <p class="modal-txt">Ваше сообщение успешно отправлено</p>
  </div>

</div>
  @endif

<!-- Preloader -->


<!--Active Page Macro on Header-->

<!-- Creating the navigation with activePage = 'home' -->

@include('partials.header2')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>


<div id="bs-wrapper">

    @yield('content')
    <!-- You write code for this content block in another file -->
</div>


<!-- Adds the Footer partial -->
@include('partials.footer2')

<!--SCRIPTS    -->
<script src="/js/main-new.js"></script>
<script>
    var navBtn = document.getElementById('modalBtn');
    $(navBtn).on('click', function() {
        $("#orderModal").show();
    });
</script>


</body>

</html>
