@extends('master')

@section('content')
<main role="main">
        <!--*************Main info************************************-->
                <div class="slider-cat rody-img">

                
                <div data-index="1"><img src="{{ asset('uploads/' . $package->image) }}">
                <h1 class="slider-heading">{{ $package->title }}</h1></div>
                  
                

                </div>
        <!--************* end Main info*******************************-->

{{-- <div class="container category-container">
	<div class="row category-row">
<div class="col-sm-6 desc pack1">
<div class="container">
<div class="album text-muted packets rody">
<div class="container packs-overlay">
   <br>
   @foreach($allPackages->chunk(2) as $packets)
   <div class="row">
      @foreach($packets as $key=>$pack)
      @if($package->id == $pack->parent_id)
      <div class="col-sm-6 pack-item">
         <a href="{{ route('package', ['packSlug' => $pack->slug]) }}">
            <img class="image-packets" id="image1" src="{{ asset('uploads/' . $pack->image) }}" alt="Card image cap">
            <div class="middle2">
               <img class="cont" src="/images/Arrow_big.png">
            </div>
            <p class="card-text pack-def2">{{ $pack->title }}</p>
         </a>
      </div>
      @endif
      @endforeach
   </div>
   <br>
   @endforeach
</div>
	</div>
	</div>
	</div>
	</div>
	</div> --}}
                                 
      <div class="container category-container">
		  
            <div class="row category-row">
                <div class="col-sm-6 desc pack1">
                    <div class="container">
                        <!--*******Description*********-->
                        <div class="row text-img">
                                {!! $package->content !!}
                                
                        {{-- <img class="img-desc" src="{{ asset('uploads/'.$package->image) }}"  width="794.84"> --}}
                                
                               
                                
                                
                            </div>
                        <img class="del-line" width="750.84" src="/images/Line%209.png">
                        <!--*******Description*********-->
                        
                        
                        <!--*******Social media*********-->
                        <div class="row social">
                            <div class="col-sm-6 heading"><p>Поделиться с друзьями:</p></div><div class="col-sm-6 icons">
                        <div class="row">   
                            <div class="col-sm-3">
                                 
                                    <a target="_blank" href="{{ $share['facebook'] }}">
                                     <img src="/images/facebook.png">
                                    </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="{{ $share['twitter'] }}">
                                    <img src="/images/twitter.png" >
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="{{ $share['vk'] }}">
                                    <img src="/images/vkontakte-logo.png" >
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl={{ urlencode(url()->current()) }}">
                                    <img src="/images/odnoklassniki-logo.png" >
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                        <!--*******Social media*********-->


                        <!--*******ZAKAZ ZVONKA*********-->
                        <div class="row ">
                            <div class="zvonok">
                                <h3 class="zakaz-headers">По всем вопросам звоните:</h3><p class="zakaz-text"> 220-70-70, 292-67-56, 293-08-77, 239-07-03, 293-09-66, +7- 777-300-00-03, 292-37-40 (бухгалтерия)</p>
                                <form id="zakaz-form" action="{{ route('form.phone') }}" method="post">
                                        {{ csrf_field() }}
                                    <h3 class="zakaz-headers">Или закажите обратный звонок:</h3>
                                    <input type="text" class="input-zvon" placeholder="Введите свой телефон" name="phone">
                                    <input type="submit" id="zakazat" name="zvonok" value="Заказать">
                                </form>
                            </div>
                        </div>
                        <!--*******ZAKAZ ZVONKA*********-->


                    
                    </div>
                </div>
                
                
                <div class="col-sm-6 sidebar pack-side">
                


                        <!-- sidebar menu -->
                      <div class="container">
                          <div class="row">
                              <!-- <div class="nav-side-menu">

                                    <div class="menu-list">

                                        <ul>

                                            @foreach($parentPackages as $parent)
                                            <li class="drop-item">
                                                <div class="plus">
                                                    <div></div>
                                                    <div></div>
                                                </div>
                                            <a>{{ $parent->title }}</a>
                                                <ul class="hidden-menu">
                                                    @foreach($allPackages as $pack)
                                                    @if($pack->parent_id == $parent->id)
                                                        <li><a href="{{ route('package', ['packSlug' => $pack->slug]) }}">{{ $pack->title }}</a></li>
                                                    @endif    
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endforeach
                                        </ul>
                                 </div>
                            </div> -->
                             
                          </div>
                             <!-- end sidebar menu --> 
                         
                         
                             <!-- packet -->  
                             {{-- <div class="row packet-type">
                                 <a href="">
                                     <div class="row"><img src="/images/Mask%20Group-2.png">
                                     <h2 class="packet-name">Пакет "Роды"</h2></div>
                                 </a>
                             </div> --}}
                            <!-- packet -->
                         
                         
                         <!-- specialists -->
                            {{-- <div class="row spec-type">
                                <div class="spec">
                                    <h2>Специалисты</h2>
                                    <a href=""> Акушер-гинеколог</a>
                                </div>
                            </div> --}}
                         <!-- specialists -->
                    </div>
                   
                 </div>
            </div>
            </div>

        <!----------------------Form for registration-------------------->
        
     <!----------------------end form for registration-------------------->
    </main>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/about.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">

@endsection