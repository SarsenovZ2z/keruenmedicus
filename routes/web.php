<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => 'locolize'], function()
{
    Route::get('/search-price', 'IndexController@searchPrice')->name('search.price');
    Route::get('/search', 'IndexController@search')->name('search');

    Route::get('/local/local', 'IndexController@local')->name('nurik');

    Route::get('/share/{type}/{url}', function($type, $url, $title) {
        return Share::load($url, $title)->services($type);
    })->name('share');
    Route::get('/packagess', 'IndexController@packages')->name('packages');
    Route::get('/old', 'IndexController@home')->name('home.old');
    Route::get('/', 'IndexController@homeNew')->name('home');
    Route::get('/doctors', 'IndexController@doctors')->name('doctors');
    Route::get('/history', 'IndexController@history')->name('history');
    Route::get('/doctors/{docSlug}', 'IndexController@doctor')->name('doctor');


    Route::get('/packagess/{packSlug}', 'IndexController@package')->name('package');


    Route::get('/checks', 'IndexController@checks')->name('checks');
    Route::get('/price', 'IndexController@price')->name('price');

    Route::get('/news', 'IndexController@news')->name('news');
    Route::get('/news/{postSlug}', 'IndexController@post')->name('post');

    Route::get('/carier', 'IndexController@carier')->name('carier');

    Route::get('/departments', 'IndexController@departments')->name('departments');
    Route::get('/departments/{depSlug}', 'IndexController@department')->name('department');
    Route::get('/gallery', 'IndexController@gallery')->name('gallery');
    Route::get('{page}/{subs?}', ['uses' => 'IndexController@index'])
        ->where(['page' => '^((?!admin).)*$', 'subs' => '.*']);

    Route::post('/phone-form', 'IndexController@phoneForm')->name('form.phone');
    Route::post('/simple-form', 'IndexController@simpleForm')->name('form.simple');




    Route::post('/fixed-form', 'IndexController@fixedForm')->name('form.fixed');
});




Route::group(['prefix' => config('backpack.base.route_prefix'), 'middleware' => ['admin'], 'namespace' => 'Admin'], function()
{
   CRUD::resource('department', 'DepartmentCrudController');
   CRUD::resource('doctor', 'DoctorCrudController');
   CRUD::resource('gallery', 'GalleryCrudController');
   CRUD::resource('package', 'PackageCrudController');
   CRUD::resource('post', 'PostCrudController');
   CRUD::resource('price', 'PriceCrudController');
   CRUD::resource('review', 'ReviewCrudController');
   CRUD::resource('sale', 'SaleCrudController');
   CRUD::resource('slider', 'SliderCrudController');
   CRUD::resource('specialization', 'SpecializationCrudController');
   CRUD::resource('check', 'CheckCrudController');
   CRUD::resource('history', 'HistoryCrudController');
   CRUD::resource('setting', 'SettingCrudController');
   CRUD::resource('vacancy', 'VacancyCrudController');

});
