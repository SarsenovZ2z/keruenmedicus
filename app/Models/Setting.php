<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Config;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;

class Setting extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'settings';
    protected $fillable = ['value'];
    public $translatable = ['value'];

    /**
     * Grab a setting value from the database.
     *
     * @param string $key The setting key, as defined in the key db column
     *
     * @return string The setting value.
     */
    public static function get($key)
    {
        $setting = new self();
        $entry = $setting->where('key', $key)->first();

        if (!$entry) {
            return;
        }

        return $entry->value;
    }

    /**
     * Update a setting's value.
     *
     * @param string $key   The setting key, as defined in the key db column
     * @param string $value The new value.
     */
    public static function set($key, $value = null)
    {
        $prefixed_key = 'settings.'.$key;
        $setting = new self();
        $entry = $setting->where('key', $key)->firstOrFail();

        // update the value in the database
        $entry->value = $value;
        $entry->saveOrFail();

        // update the value in the session
        Config::set($prefixed_key, $value);

        if (Config::get($prefixed_key) == $value) {
            return true;
        }

        return false;
    }

    public function setValueAttribute($value)
    {
        if (json_decode($this->attributes['field'])->type == "upload") {
            $attribute_name = "value";
            $disk = "uploads";
            $destination_path = "pdf";

            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
            $this->attributes['value'] = $destination_path."/".$value->getClientOriginalName();
        }
        else {
            $this->attributes['value'] = $value;
        }
    }



}
