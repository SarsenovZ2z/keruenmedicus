@extends('master') @section('content')
<main role="main">

    <div class="sliderWrap">
        <div class="single-item">
            @foreach($sliders as $slider)
            <div>
                <div class="slide-words">
                <h1 class="slide-head">{{ $slider->title }}</h1>
                <p class="slide-text">{{ $slider->content }}</p>
                @if(isset($slider->link))
                <button class="slider-btn"><a href="{{ $slider->link }}">Подробнее</a></button>
                @endif
                </div>
                <div class="slide-overlay"></div>
            <img class="slide-image" src="{{ asset('uploads/' . $slider->image) }}" alt="{{ $slider->title }}">

            </div>
            @endforeach

        </div>
    </div>


    <div class="album text-muted o-nas">
        <div class="container">

            <div class="row facts">
                <div class="col-sm-3">
                <h2 class="white-txt heading">{{ $page->block_1_title_1 }}</h2>
                    <p class="white-txt facts-txt">{{ $page->block_1_heading_1 }}</p>
                </div>
                <div class="col-sm-3">
                    <h2 class="white-txt heading">{{ $page->block_1_title_2 }}</h2>
                    <p class="white-txt facts-txt">{{ $page->block_1_heading_2 }}</p>
                </div>
                <div class="col-sm-3">
                    <h2 class="white-txt heading">{{ $page->block_1_title_3 }}</h2>
                    <p class="white-txt facts-txt">{{ $page->block_1_heading_3 }}</p>
                </div>
                <div class="col-sm-3">
                    <h2 class="white-txt heading">{{ $page->block_1_title_4 }}</h2>
                    <p class="white-txt facts-txt">{{ $page->block_1_heading_4 }}</p>
                </div>
            </div>
        </div>

    </div>


   <div class="album nap">
        <div class="container naprav">
          <h2 class="kind">Выберите направление</h2>
          <div class="one-times">
            @foreach($departments->chunk(5) as $hi=>$items)
            <div class="row {{ ($hi == '0') ? 'napravlenie' : '' }}">
              @foreach($items as $key=>$item)
              <div class="col-sm-2">
                  <a href="{{ route('department', ['depSlug' => $item->slug]) }}">
                    <div class="overlay">
                      <img id="img{{ $key }}" src="{{ asset('uploads/' . $item->icon) }}" alt="{{ $item->title }}">
                    </div>
                  <p class="card-text1">{{ $item->title }}</p></a>
              </div>
              @endforeach
            </div>
            @endforeach
            </div>
        </div>
      </div>
	<div class="sliderMobile">
        <h2 class="kind">Направление</h2>
        <div class="one-time">
			@foreach($departments as $key=>$dep)
          <div>

            <a href="{{ route('department', ['depSlug' => $dep->slug]) }}">

              <div class="overlay">
                <img id="img{{$key}}" src="{{ asset('uploads/' . $dep->icon) }}" alt="{{ $dep->title }}">
              </div>
              <p class="card-text1">{{ $dep->title }}</p>

            </a>

          </div>
			@endforeach
        </div>
      </div>
    <div class="album text-muted packets">
        <div class="container ">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="white-txt pack-head-txt">{{ $page->block_2_title_1 }}</h2>
                </div>
                <div class="col-sm-6">
                    <div class="white-txt pack-parag-txt">
                        {{ $page->block_2_description_1 }}
                    </div>
                </div>
            </div>
        </div>
        <div class="container packs-overlay">
            @foreach($packages->chunk(2) as $items)
            <div class="row">
                @foreach($items as $key=>$item)
                <div class="col-sm-6 pack-item">
                <a href="{{ route('package', ['packSlug' => $item->slug]) }}">
                <img class="image-packets" id="image{{ $key }}" src="{{ isset($item->thumbnail) ? asset('uploads/' .  $item->thumbnail) :  asset('uploads/' .  $item->image)}}" alt="{{ $item->title }}">
                <div class="middle">
                        <img class="cont" src="{{ asset('images/Arrow_big.png') }}">
                        </div>
                    <p class="card-text">{{ $item->title }}</p>
                    </a>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
		<button class="slider-btn"><a href="{{ route('packages') }}">Подробнее</a></button>

    </div>

    <div class="album text-muted check">
        <div class="container">
            <div class="row check-head">
                <div class="col-sm-6">
                    <h2 class="check-heading">{{ $page->block_3_title_1 }}</h2>
                </div>
                <div class="col-sm-6">
                    <div>
                            {{ $page->block_3_description_1 }}
                    </div>
                </div>
            </div>

            <div class="multiple-items typess">
                @foreach($checks as $item)
                <div class="check-types ">

                    <a class="pack-link" href="{{ route('checks', ['ids' => $item->id]) }}">
                        <div class="all-link">
                        <p class="check-link">{{ $item->title }}</p>
                            <div class="packet-text">{!! $item->description !!}</div>
                        <img class="resp-img" src="{{ asset('images/Group%2038.png') }}">
                        </div>
                    </a>


                </div>
                @endforeach

            </div>



            <br>
        </div>

    </div>




    {{-- <div class="album text-muted otzyvy">
        <div class="container">
            <h2>Отзывы клиентов.</h2>
            <div class="variable-width">
                <div>
                    <div id="otzyv1" class="card" style="width: 20rem;">
                        <img class="card-img-top" src="images/Object.png" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Елена Романова</h4>
                            <p class="card-text2">По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у
                                некторых людей недоумение при попытках прочитать рыбу текст. </p>

                        </div>
                    </div>
                </div>
                <div>
                    <div id="otzyv2" class="card" style="width: 20rem;">
                        <img class="card-img-top" src="images/Object-2.png" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Олег Мороз</h4>
                            <p class="card-text2">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                                менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных
                                выступлений в домашних условиях. </p>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="otzyv3" class="card" style="width: 20rem;">
                        <img class="card-img-top" src="images/Object-3.png" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Григорий Панов</h4>
                            <p class="card-text2">По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у
                                некторых людей недоумение при попытках прочитать рыбу текст. </p>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="otzyv4" class="card" style="width: 20rem;">
                        <img class="card-img-top" src="images/Object.png" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Елена Романова</h4>
                            <p class="card-text2">По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у
                                некторых людей недоумение при попытках прочитать рыбу текст. </p>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="otzyv5" class="card" style="width: 20rem;">
                        <img class="card-img-top" src="images/Object-2.png" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Олег Мороз</h4>
                            <p class="card-text2">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                                менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных
                                выступлений в домашних условиях. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> --}}
    <div id="in-modal" class="modal">

        <div class="modal-content">
            <span class="x">&times;</span>
            <p>Спасибо, Вашa заявка принята! С вами свяжутся в ближайшее время.</p>
        </div>
    </div>


</main>
@endsection
