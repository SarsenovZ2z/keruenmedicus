<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    {!! SEO::generate() !!}
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}"/>
    <link href="{{ asset('css/album.css') }}" rel="stylesheet">
    <meta name="_token" content="{{ csrf_token() }}">
    @yield('css')

  </head>

  <body>
	  <div id="myModal" class="modal">

      <div class="modal-content">
        <span class="close">&times;</span>
        <iframe src="https://company.medelement.com/%D0%BC%D0%B5%D0%B4%D0%B8%D1%86%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9-%D1%86%D0%B5%D0%BD%D1%82%D1%80-%D0%BA%D0%B5%D1%80%D1%83%D0%B5%D0%BD-medicus-%D0%B0%D0%BB%D0%BC%D0%B0%D1%82%D1%8B?keep_sections[]=online-entry" frameborder="0" border="0" width="100%" height="100%"></iframe>

      </div>

    </div>
	  @if(Session::has('flash_message_fixed'))

	   <div id="myModal2" class="modal" style="display:block">

      <div class="modal-content2">
        <span class="close2">&times;</span>
        <p class="modal-txt">Ваше сообщение успешно отправлено</p>
      </div>

    </div>
	  @endif
     @include('partials.header2')
	 <div class="call-us">
        <img src="/images/phone.png" alt="">
     </div>
	 <div class="orderWithWrap">
	 	<span class="close3">&times;</span>
	   <div class="orderWith">
        <div class="row orderRow">
          <div class="col-sm-2">
            <img src="/images/message.png" alt="">
          </div>
          <div class="col-sm-10">
            <h4 class="orderHeading">Связаться с нами</h4>
          </div>
        </div>
        <div class="orderForm">
          <form action="{{ route('form.fixed') }}" method="POST" enctype="multipart/form-data">
			  {{ csrf_field() }}
            <input class="orderInput" type="text" name="name" id="" placeholder="Имя">
            <input class="orderInput" type="text" name="email" id="" placeholder="Email или контакт">
            <textarea class="orderTextarea" name="content" id="" rows="10" placeholder="Текст сообщения"></textarea>
            <div class="orderAttach">
              <input type="file" class="orderFile" name="file" id="">
              <label for="file"><img src="/images/screp.png" alt=""><span>Прикрепить файл</span></label>
            </div>
            <input type="submit" class="orderSend" name="" id="" value="Отправить">
          </form>
        </div>
      </div>
	  </div>
    @yield('content')

    @include('partials.footer2')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
      <script src="{{ asset('js/slider.js') }}" ></script>
      <script src="{{ asset('js/script.js') }}" ></script>
      <script src="{{ asset('js/burger.js') }}" ></script>
      <script src="{{ asset('js/toogle.js') }}" ></script>
      @yield('after_jquery')
      <script>
    var navBtn = document.getElementById('modalBtn');
    $(navBtn).on('click', function() {
        $("#myModal").show();
    });
</script>
  </body>
</html>
